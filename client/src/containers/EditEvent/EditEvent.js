import React, {Component} from 'react';
import {connect} from "react-redux";
import CreateEventForm from "../../components/EventForm/CreateEventForm";
import {editEvent, fetchSingleEvent} from "../../store/actions/eventsActions";
import GroupControlPanel from "../../components/GroupControlPanel/GroupControlPanel";
import {FormattedMessage} from "react-intl";

class EditEvent extends Component {
    componentDidMount() {
        this.props.fetchSingleEvent(this.props.match.params.id)
    }

    editEventHandler = eventData => {
        this.props.editEvent(this.props.match.params.id, eventData)
    };

    render() {
        let eventForm = <CreateEventForm
            onSubmit={this.editEventHandler}
            event={this.props.event}
            edit
        />;

        if (!this.props.event) {
            eventForm =
                    <div>
                        <FormattedMessage id="loading"/>
                    </div>
        }

        return (
                <div>
                    {this.props.event && <GroupControlPanel/>}
                    <h4><FormattedMessage id="editingCourseInformation"/></h4>
                    {eventForm}
                </div>
        );
    }
}

const mapStateToProps = state => ({
    event: state.events.singleEvent,
});

const mapDispatchToProps = dispatch => ({
    fetchSingleEvent: id => dispatch(fetchSingleEvent(id)),
    editEvent: (eventId, eventData) => dispatch(editEvent(eventId, eventData))
});

export default connect(mapStateToProps,mapDispatchToProps)(EditEvent);