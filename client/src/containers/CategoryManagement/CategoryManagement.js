import React, {Component} from 'react';
import {deleteCategory, fetchCategories} from "../../store/actions/categoriesActions";
import {connect} from "react-redux";
import {Button, Container, Table} from "reactstrap";
import { FormattedMessage } from "react-intl";
import {Link} from "react-router-dom";

import './CategoryManagement.css';

class CategoryManagement extends Component {
    componentDidMount() {
        this.props.fetchCategories();
    }


    render() {
        return (
            <Container>
                <div className="Category-management-block">
                    {this.props.user && this.props.user.role === 'admin' ?
                        <Link to={'/new_category'}>
                            <Button color="primary"><FormattedMessage id="addCategory"/></Button>
                        </Link>: null}
                    <Table striped className="Category-table">
                        <thead>
                        <tr>
                            <th>Категории на русском</th>
                            <th>Кыгрыз категориясы</th>
                            <th>Categories in English</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.props.categories.map(category => {
                                return (
                                    <tr key={category._id}>
                                        <td>{category.titleRu}</td>
                                        <td>{category.titleKy}</td>
                                        <td>{category.titleEn}</td>
                                        <td>
                                            {this.props.user && this.props.user.role === 'admin' ?  <Button close onClick={() => this.props.deleteCategory(category._id)} /> : null}
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </Table>
                </div>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    fetchCategories: () => dispatch(fetchCategories()),
    deleteCategory: (categoryId) => dispatch(deleteCategory(categoryId))
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryManagement);