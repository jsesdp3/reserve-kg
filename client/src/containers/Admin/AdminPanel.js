import React, {Component, Fragment} from 'react';
import {patchUser, fetchAdminUsers} from "../../store/actions/adminActions";
import {connect} from "react-redux";
import {Card, CardHeader, Col, FormGroup, Input, Label, Row, Table} from "reactstrap";
import Container from "reactstrap/es/Container";
import {FormattedMessage} from "react-intl";

class AdminPanel extends Component {
  componentDidMount() {
    this.props.onFetchUsers();
  }

  render() {
    return (
          <Fragment>
            <Container>
            <Card style={{marginBottom: '30px'}}>
              <CardHeader>
                <h1><FormattedMessage id="users"/></h1>
              </CardHeader>
              <Table>
                <thead>
                <tr>
                  <th>#</th>
                  <th><FormattedMessage id="name"/></th>
                  <th><FormattedMessage id="email"/></th>
                  <th><FormattedMessage id="phone"/></th>
                  <th><FormattedMessage id="role"/></th>
                </tr>
                </thead>
                {this.props && this.props.adminUsers.map((user, index) => {
                  return (
                <tbody key={user._id}>
                <tr>
                  <th scope="row">{index + 1}</th>
                  <td>{user.displayName}</td>
                  <td>{user.email}</td>
                  <td>{user.phone}</td>
                  <td>
                    <FormGroup>
                      <Row>
                        <Col sm={3}>
                          <FormGroup check>
                            <Label check>
                              <Input
                                type="radio"
                                checked={user.role === 'admin'}
                                onChange={() => this.props.onUserPatch(user._id, 'admin')}
                              />
                              {'admin'}
                            </Label>
                          </FormGroup>
                        </Col>
                        <Col sm={3}>
                          <FormGroup check disabled>
                            <Label check>
                              <Input
                                type="radio"
                                checked={user.role === 'user'}
                                disabled={this.props.user._id === user._id}
                                onChange={() => this.props.onUserPatch(user._id, 'user')}
                              />
                              {'user'}
                            </Label>
                          </FormGroup>
                        </Col>
                      </Row>
                    </FormGroup>
                  </td>
                </tr>
                </tbody>
                  )
                })}
              </Table>
            </Card>
            </Container>
          </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  adminUsers: state.adminUsers.adminUsers,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onFetchUsers: () => dispatch(fetchAdminUsers()),
  onUserPatch: (id, role) =>dispatch(patchUser(id, role)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminPanel);