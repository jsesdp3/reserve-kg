import React, {Component} from 'react';
import {connect} from "react-redux";

import {createGroup, fetchSingleEvent} from "../../store/actions/eventsActions";
import GroupForm from "../../components/GroupForm/GroupForm";

import "./NewGroup.css";
import GroupControlPanel from "../../components/GroupControlPanel/GroupControlPanel";
import {FormattedMessage} from "react-intl";

class NewGroup extends Component {
    componentDidMount(){
        this.props.fetchSingleEvent(this.props.match.params.id);
    };
    createGroup = (id, eventData) =>{
        this.props.createGroup(id, eventData);
    };

    render() {
        return (
                <div className="Group-form-block">
                    {this.props.event && <GroupControlPanel/>}
                    <h2 className="Group-form-title"><FormattedMessage id="addNewGroup"/></h2>
                    <GroupForm
                        onSubmit={this.createGroup}
                        id={this.props.match.params.id}
                    />
                </div>
        );
    }
}
const mapStateToProps = state => ({
    event: state.events.singleEvent,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    createGroup: (id, eventData) => dispatch(createGroup(id, eventData)),
    fetchSingleEvent: (id) => dispatch(fetchSingleEvent(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewGroup);