import React, {Component, Fragment} from 'react';
import {registerUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import Form from "reactstrap/es/Form";
import {Alert, Button, Col, FormGroup, Input, Label} from "reactstrap";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";
import inputMask from 'react-input-mask'
import GoogleLogin from "../../components/GoogleLogin/GoogleLogin";
import Container from "reactstrap/es/Container";
import {FormattedMessage} from "react-intl";
import OrganizationForm from "../Organization/OrganizationForm";
import {createOrganization} from "../../store/actions/organizationsActions";

class Register extends Component {
    state = {
        displayName: '',
        email: '',
        password: '',
        phone: '',
        avatarImage: '',
        isOrganization: false,
        organization: {
            name: '',
            description: '',
            address: '',
            logo: '',
            location: '',
            gallery: []
        }
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    organizationChangeHandler = event => {
        const value = event.target.files ? event.target.files[0] : event.target.value;

        const organization = {
            ...this.state.organization,
            [event.target.name]: value
        };

        this.setState({organization});
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = (event) => {
        event.preventDefault();

        const userFormData = new FormData();

        Object.keys(this.state).forEach(key => {
            if (key !== 'organization' && key !== 'isOrganization') {
                userFormData.append(key, this.state[key])
            }
        });

        this.props.registerUser(userFormData).then(() => {
            if (this.state.isOrganization) {
                const organizationFormData = new FormData();

                for (let i = 0; i < this.state.organization.gallery.length; i++) {
                    organizationFormData.append('gallery', this.state.organization.gallery[i]);
                }

                Object.keys(this.state.organization).forEach(key => {
                    if (key === 'gallery') return;
                    organizationFormData.append(key, this.state.organization[key]);
                });

                this.props.createOrganization(organizationFormData);
            }
        });
    };

    checkBoxHandler = (event) =>{
      this.setState({isOrganization: event.target.checked})
    };

    mapClickHandler = event => {
        const organization = {
            ...this.state.organization,
            markerLocation: event.latlng,
            location: event.latlng.lat + "," + event.latlng.lng
        };

        this.setState({organization});
    };

    galleryChangeHandler = event => {
        event.preventDefault();

        let files = Array.from(event.target.files);

        files.forEach((file) => {
            let reader = new FileReader();

            reader.onloadend = () => {
                const organization = {...this.state.organization};
                organization.gallery = [...this.state.organization.gallery, file];
                this.setState({organization});
            };

            reader.readAsDataURL(file);
        });
    };

    render() {
        const orgFormConfig = {
            name: {
                value: this.state.organization.name,
                onChange: this.organizationChangeHandler
            },
            description: {
                value: this.state.organization.description,
                onChange: this.organizationChangeHandler
            },
            address: {
                value: this.state.organization.address,
                onChange: this.organizationChangeHandler
            },
            logo: {
                onChange: this.organizationChangeHandler
            },
            location: {
                center: [42.87617,74.614606],
                markerLocation: this.state.organization.markerLocation,
                onClick: this.mapClickHandler
            },
            gallery: {
                onChange: this.galleryChangeHandler
            }
        };

        return (
                 <Fragment>
                     <Container>
                     <h2><FormattedMessage id="registration"/></h2>
                     {this.props.error && !this.props.error.errors &&  (
                         <Alert color="danger">{this.props.error.error || this.props.error.global}</Alert>
                     )}
                     <Form onSubmit={this.submitFormHandler}>

                         <FormElement
                             propertyName="displayName"
                             title={<FormattedMessage id='yourName'/>}
                             type="text"
                             value={this.state.displayName}
                             onChange={this.inputChangeHandler}

                         />

                         <FormElement
                             propertyName="email"
                             title={<FormattedMessage id="email"/>}
                             type="text"
                             value={this.state.email}
                             onChange={this.inputChangeHandler}

                         />

                         <FormElement
                             propertyName="password"
                             title={<FormattedMessage id="password"/>}
                             type="password"
                             value={this.state.password}
                             onChange={this.inputChangeHandler}
                         />

                         <FormElement
                             propertyName="phone"
                             title={<FormattedMessage id="phone"/>}
                             mask="+\9\96999999999"
                             tag={inputMask}
                             maskChar="_"
                             type="text" required
                             value={this.state.phone}
                             onChange={this.inputChangeHandler}
                         />

                         <FormElement
                             propertyName="avatarImage"
                             title={<FormattedMessage id="photo"/>}
                             type="file"
                             onChange={this.fileChangeHandler}
                         />

                         <FormGroup row>
                             <Col sm={{offset: 2, size: 10}}>
                                 <FormGroup check inline>
                                     <Label check>
                                         <Input type="checkbox" name="isOrganization" onChange={this.checkBoxHandler}/>
                                         <FormattedMessage id="companyRepresentative"/>
                                     </Label>
                                 </FormGroup>
                             </Col>
                         </FormGroup>

                         {this.state.isOrganization && (
                             <OrganizationForm formConfig={orgFormConfig} />
                         )}

                         <FormGroup row>
                             <Col sm={{offset: 2, size: 10}}>
                                 <Button type="submit" color="primary">
                                     <FormattedMessage id="registration"/>
                                 </Button>
                             </Col>
                         </FormGroup>

                         <FormGroup row>
                             <Col sm={{offset: 2, size: 10}}>
                                 <FacebookLogin/>
                                 <GoogleLogin/>
                             </Col>
                         </FormGroup>
                     </Form>
                     </Container>
                 </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData)),
    createOrganization: organizationData => dispatch(createOrganization(organizationData, false))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);