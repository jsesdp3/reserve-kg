import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteGroup, fetchGroupsOnEvent} from "../../store/actions/eventsActions";
import {Button, Card, CardBody, CardHeader, CardText, CardTitle, Table} from "reactstrap";
import WEEKDAYS_TO_NUMBERS from '../../WEEKDAYS_TO_NUMBERS'
import './GroupPage.css';
import GroupControlPanel from "../../components/GroupControlPanel/GroupControlPanel";
import {FormattedMessage} from "react-intl";

class GroupPage extends Component {
    componentDidMount() {
        this.props.fetchGroupsOnEvent(this.props.match.params.id);
    };
    render() {

        return (
    <Fragment>
        {this.props.event && <GroupControlPanel/>}
        <h1><FormattedMessage id="groupViewPage"/></h1>

      {this.props.event && this.props.event.groups.map(group => {
        return (
        <Card key={group._id} style={{marginBottom: '30px'}}>
          <CardHeader>
              <p>{group.name}</p>
              <Button onClick={() => this.props.deleteGroup(group, this.props.match.params.id)}><FormattedMessage id="delete"/></Button>
          </CardHeader>
          <CardBody>
            <CardTitle><FormattedMessage id="startClassesIn"/> {group.time}</CardTitle>
            <CardText>{group.weekDays.map(days => WEEKDAYS_TO_NUMBERS[days]).join(' , ')}</CardText>
          </CardBody>
          <Table>
            <thead>
            <tr>
              <th>#</th>
              <th><FormattedMessage id="name"/></th>
              <th><FormattedMessage id="email"/></th>
              <th><FormattedMessage id="phone"/></th>
            </tr>
            </thead>
            {group.users.map((user, index) => {
              return (
                <tbody key={index}>
                <tr>
                  <th scope="row">{index + 1}</th>
                  <td>{user.displayName}</td>
                  <td>{user.email}</td>
                  <td>{user.phone}</td>
                </tr>
                </tbody>
              )
            })}
          </Table>
        </Card>
          )
      })}

    </Fragment>
    );
    }
}

const mapStateToProps = state => ({
    event: state.events.singleEvent,
    user: state.users.user
});

const mapDispatchToProps = dispatch => {
    return {
        fetchGroupsOnEvent: id => dispatch(fetchGroupsOnEvent(id)),
        deleteGroup: (groupId, eventId) => dispatch(deleteGroup(groupId, eventId))
       };
};
export default connect(mapStateToProps,mapDispatchToProps)(GroupPage);
