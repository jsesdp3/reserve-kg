import React, {Component, Fragment} from 'react';
import FormElement from "../../components/UI/Form/FormElement";
import {Button, Col, Form, FormGroup} from "reactstrap";
import {createCategories} from "../../store/actions/categoriesActions";
import {connect} from "react-redux";
import Container from "reactstrap/es/Container";

import {FormattedMessage} from "react-intl";

class CreateCategoryForm extends Component {
    state = {
        titleRu: '',
        titleEn: '',
        titleKy: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.createCategories({...this.state})
    };
    render() {
        return (
               <Fragment>
                   <Container>
                  <FormattedMessage tagName='h2' id="addCategory"/>
                   <Form onSubmit={this.submitFormHandler}>
                       <FormElement
                           propertyName="titleRu"
                           title=<FormattedMessage id="categoryNamesRussian"/>
                           onChange={this.inputChangeHandler}
                       />
                       <FormElement
                           propertyName="titleEn"
                           title=<FormattedMessage id="categoryNamesEnglish"/>
                           onChange={this.inputChangeHandler}
                       />
                       <FormElement
                           propertyName="titleKy"
                           title=<FormattedMessage id="categoryNamesKyrgyz"/>
                           onChange={this.inputChangeHandler}
                       />

                       <FormGroup>
                           <Col sm={{offset: 2, size: 10}}>
                               <Button type="submit" color="primary"><FormattedMessage id="add"/></Button>
                           </Col>
                       </FormGroup>
                   </Form>
                   </Container>
               </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createCategories: data => dispatch(createCategories(data))
});

export default connect(null, mapDispatchToProps)(CreateCategoryForm);