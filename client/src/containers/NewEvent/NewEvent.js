import React, {Component,Fragment} from 'react';
import {connect} from "react-redux";

import CreateEventForm from "../../components/EventForm/CreateEventForm"
import {createEvent} from "../../store/actions/eventsActions";
import Container from "reactstrap/es/Container";
import {FormattedMessage} from "react-intl";

class NewEvent extends Component {
    createEvent = eventData =>{
        this.props.onEventCreated(eventData).then(()=>{
            this.props.history.push('/')
        });
    };

    render() {
        return (
                <Fragment>
                    <Container>
                        <h2><FormattedMessage id="addNewCourse"/></h2>
                        <CreateEventForm onSubmit={this.createEvent}/>
                    </Container>
                </Fragment>
        );
    }
}
const mapDispatchToProps = dispatch =>({
    onEventCreated: eventData =>dispatch(createEvent(eventData))
});
export default connect(null,mapDispatchToProps)(NewEvent);