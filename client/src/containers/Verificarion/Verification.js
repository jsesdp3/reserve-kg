import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {
    getVerificationUser,
    postVerificationSms,
    putCodeToChangeVerification
} from "../../store/actions/verificationAction";
import {FormattedMessage} from "react-intl";

import "./Verification.css";


class Verification extends Component {
    state = {
        verificationCode: '',
        status: true,
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.putCodeToChangeVerification({...this.state}, this.props.match.params.id);
        this.props.getVerificationUser(this.props.match.params.id)
    };

    statusHandler = () => {
      this.setState({status: false});
    };

    clickGetCode = () => {
        this.props.postVerificationSms(this.props.user);
        this.statusHandler();
    };

    render() {
        let buttonIsDisabled = false;

            if (this.state.status === false) {
                buttonIsDisabled = true;
            }

        return (
            <div className="Verification">
                <h1>Verification</h1>
                <h2>Здравствуйте, <b>{this.props.user.displayName}</b></h2>
                <p>Для завершения регистрации вам необходимо пройти верефикацию,
                    после нажатия кнопки "Запртосить код",вам придет смс с кодом,
                    который необходимо ввести в поле ниже и нажать кнопку "Подтвердить код".
                    После чего вы получите возможность записываться на курсы и возможно даже создовать свои собственные.
                </p>

                {buttonIsDisabled === true && this.props.user.verificationPassword !== '' ?
                    <Form onSubmit={this.submitFormHandler}>
                        <Col sm={3}>
                            <FormGroup>
                                <Label for="verificationCode"><FormattedMessage id="verificationCode"/></Label>
                                <Input
                                    type="text"
                                    name="verificationCode"
                                    id="verificationCode"
                                    value={this.state.verificationCode}
                                    onChange={this.inputChangeHandler}
                                />
                            </FormGroup>
                            <Button>Подтвердить код</Button>
                        </Col>
                    </Form> : null
                }

                {this.state.status === true ?
                    <Button
                        disabled={buttonIsDisabled}
                        onClick={this.clickGetCode}
                    >
                        {buttonIsDisabled ? 'Запрос осуществлен' : <FormattedMessage id="getCode"/>}
                    </Button> : null
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    postVerificationSms: data => dispatch(postVerificationSms(data)),
    putCodeToChangeVerification: (data, id) => dispatch(putCodeToChangeVerification(data,id)),
    getVerificationUser: id => dispatch(getVerificationUser(id)),
});

export default connect(mapStateToProps,mapDispatchToProps)(Verification);