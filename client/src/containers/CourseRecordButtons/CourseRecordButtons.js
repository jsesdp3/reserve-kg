import React, {Component, Fragment} from 'react';
import './CourseRecordButtons.css'
import {Button, CustomInput, FormGroup, Table} from "reactstrap";
import {subscribeToEvent} from "../../store/actions/eventsActions";
import {connect} from "react-redux";
import WEEKDAYS_TO_NUMBERS from '../../WEEKDAYS_TO_NUMBERS';
import {FormattedMessage} from "react-intl";


class CourseRecordButtons extends Component {
    state = {
        group: ''
    };

    inputChangeHandler = value => {
        this.setState({
            group: value
        })
    };


    render() {
        if (this.props.event === null ) return null;

        let disabled = this.props.buttonIsDisabled;

        return (

            <Fragment>
                <div className="title-group">
                    <h3><FormattedMessage id="selectGroup"/></h3>
                </div>
                <Table className="table-width">
                    <tbody>
                    {this.props.event &&  this.props.event.groups.map(group => (
                        <tr key={group._id}>
                            <td>
                                <FormGroup>
                                    <div key={group._id}>
                                        <CustomInput
                                            onChange={(event) => this.inputChangeHandler(event.target.value)}
                                            value={group._id}
                                            type="radio"
                                            id={group._id}
                                            name="group"
                                            label={group.name}
                                        />
                                    </div>
                                </FormGroup>
                            </td>
                             <td>{group.weekDays.map(num => WEEKDAYS_TO_NUMBERS[num]).join(' , ')}</td>

                            <td><FormattedMessage id="selectGroup"/> {group.time}</td>
                        </tr>
                    ))}
                    </tbody>
                </Table>

                <Button
                    color="success"
                    className="EventPage-button"
                    onClick={()=> this.props.subscribeToEvent(this.state.group, this.props.event._id)}
                    disabled={disabled}
                >
                    {disabled ? <FormattedMessage id="alreadyEnrolled"/> : <FormattedMessage id="signUp"/>}
                </Button>
            </Fragment>


        );
    }
}

const mapDispatchToProps = dispatch => ({
    subscribeToEvent: (id, eventId) => dispatch(subscribeToEvent(id, eventId)),

});

export default connect(null, mapDispatchToProps)(CourseRecordButtons);