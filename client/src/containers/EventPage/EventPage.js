import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchSingleEvent} from "../../store/actions/eventsActions";
import {Map, Marker, Popup, TileLayer} from "react-leaflet";
import config from "../../config";
import Moment from "react-moment";
import CourseRecordButtons from "../CourseRecordButtons/CourseRecordButtons";
import GroupControlPanel from "../../components/GroupControlPanel/GroupControlPanel";

import "./EventPage.css"
import {Alert} from "reactstrap";
import ShareButtons from "../../components/UI/ShareButtons";

import {FormattedMessage} from "react-intl";
import Container from "reactstrap/es/Container";


class EventPage extends Component {
    getTotalUser = () => {
        this.totalUser = 0;
        this.props.event.groups.map(item => {
            return this.totalUser += item.users.length;
        });
    };

    getFreePlaces = () => {
        this.getTotalUser();
        return this.props.event.maxPlaces - this.totalUser;
    };

    getProfit = () => {
        this.getTotalUser();
        return this.props.event.priceMonth * this.totalUser
    };

    componentDidMount() {
        this.props.fetchSingleEvent(this.props.match.params.id);
    }

    render() {
        if (this.props.event === null) return null;
        let location = this.props.event.location.coordinates.reverse();

        let buttonIsDisabled = false;

        for (let group of this.props.event.groups) {
            if (this.props.user && group.users.includes(this.props.user._id)) {
                buttonIsDisabled = true;
                break;
            }
        }
        return (
            <Fragment>
                <Container>
                <GroupControlPanel/>
                <h2 className="EventPage-title">{this.props.event.title}</h2>


                {
                    this.getFreePlaces() > 0 ?
                        <Alert color="success" className="Free-places">
                            <strong>
                                <FormattedMessage id="freePlaces"/>
                            </strong> {this.getFreePlaces()}
                        </Alert>
                        : <Alert color="danger" className="Free-places-end">
                            <strong>
                                <FormattedMessage id="noFreePlaces"/>
                            </strong>
                        </Alert>
                }
                {
                    this.props.user && this.props.user._id === this.props.event.user &&
                    <p className="Profit"><strong><FormattedMessage id="totalIncome"/> </strong>{this.getProfit()} сом</p>
                }
                <img src={config.apiURL + '/uploads/' + this.props.event.image}
                     className="EventPage-image" alt={this.props.event.title}
                />

                <p className="EventPage-startdate"><FormattedMessage id="startDate"/> <Moment
                    format='DD.MM.YYYY'>{this.props.event.startDate}</Moment></p>

                <p className="EventPage-priceMonth"><FormattedMessage id="coursePrice"/> {this.props.event.priceMonth}</p>

                <ul className="EventPage-places">
                    <li><span><FormattedMessage id="minStudent"/></span> {this.props.event.minPlaces}</li>
                    <li><span><FormattedMessage id="requiredStudent"/></span> {this.props.event.startPlaces}</li>
                    <li><span><FormattedMessage id="maxStudent"/></span> {this.props.event.maxPlaces}</li>
                </ul>

                <div className="EventPage-description">
                    <h4><FormattedMessage id="description"/></h4>
                    <p>{this.props.event.description}</p>
                </div>

                <Map className="EventPage-Map" center={location} zoom={16}>
                    <TileLayer attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                               url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                    />
                    <Marker position={location}>
                        <Popup>
                            {this.props.event.address}
                        </Popup>
                    </Marker>
                </Map>

                <ShareButtons eventId={this.props.event._id}/>

                {this.props.event.groups.length > 0 ? (
                    <Fragment>
                        {this.props.user ? (
                            <CourseRecordButtons
                                event={this.props.event}
                                buttonIsDisabled={buttonIsDisabled}
                            />
                        ) : <div className="user-header"><h4><FormattedMessage id="mustBeARegistered"/></h4></div>}
                    </Fragment>
                ) : null}

                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    event: state.events.singleEvent,
    newEvent: state.events.singleNewEvent,
    user: state.users.user
});
const mapDispatchToProps = dispatch => {
    return {
        fetchSingleEvent: (id) => dispatch(fetchSingleEvent(id)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(EventPage);