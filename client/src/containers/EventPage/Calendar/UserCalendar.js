import React, {Component,Fragment} from 'react';
import Calendar from "./Calendar";
import {fetchUserEvents} from "../../../store/actions/eventsActions";
import {connect} from "react-redux";
import "./UserCalendar.css"
import PopUp from "./PopUp";
import {FormattedMessage} from "react-intl";

class UserCalendar extends Component {

    componentDidMount(){
        this.props.fetchUserEvents(this.props.match.params);
    };

    state={
        selectedCalendarEvent: null
    };

    hideModal = () =>{
        this.setState({selectedCalendarEvent:null});
    };

    showModal = info => {
      this.setState({selectedCalendarEvent:info.event})
    };


    render() {
        const userEvents = this.props.userEvents.map(e =>{
           return {...e,title:e.event.title}
        });
        return (
            <Fragment>
                <PopUp event={this.state.selectedCalendarEvent} hideModal={this.hideModal}/>
               {
                    this.props.userEvents.length > 0 ?
                        <Calendar events={userEvents} onEventClick={this.showModal}/>
                        :
                        <div>
                            <FormattedMessage id="thereAreNoEventsYet"/>
                        </div>
                }
            </Fragment>
        );
    }
}
const mapStateToProps = state => ({
    userEvents: state.events.userEvents,
    user: state.users.user
});
const mapDispatchToProps = dispatch =>({
    fetchUserEvents: () => dispatch(fetchUserEvents())
});
export default connect(mapStateToProps,mapDispatchToProps)(UserCalendar);
