import React, {Fragment} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Moment from "react-moment";
import {Map, Marker, Popup, TileLayer} from "react-leaflet";
import {FormattedMessage} from "react-intl";

const PopUp = ({event,hideModal}) => {
    const coordinates = event && event.extendedProps.event.location.coordinates.reverse();
    return (
        <Modal isOpen={!!event} toggle={hideModal}>
            {event &&
            <Fragment>
                <ModalHeader toggle={hideModal}><FormattedMessage id="eventInformation"/></ModalHeader>
                <ModalBody>
                    <h2>{event.extendedProps.event.title}</h2>
                    <p><b><FormattedMessage id="group"/>:</b> {event.extendedProps.groupTitle}</p>
                    <div>
                        <h4><FormattedMessage id="event"/>:</h4>
                        <p>
                            <b><Moment element="span" format='DD.MM.YYYY в HH:mm'>{event.start}</Moment></b>&nbsp;
                            <FormattedMessage id="byTheAddress"/> {event.extendedProps.event.address}
                        </p>

                        <Map className="CalendarModal-Map" center={coordinates} zoom={16}>
                            <TileLayer attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                       url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                            />
                            <Marker position={coordinates}>
                                <Popup>
                                    {event.extendedProps.event.address}
                                </Popup>
                            </Marker>
                        </Map>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={hideModal}>OK</Button>
                </ModalFooter>
            </Fragment>
            }
        </Modal>


    );
};

export default PopUp;