import React, {Component, Fragment} from 'react';

import {connect} from "react-redux";
import {fetchCalendarEvents, fetchSingleEvent} from "../../../store/actions/eventsActions";
import GroupControlPanel from "../../../components/GroupControlPanel/GroupControlPanel";
import Calendar from "./Calendar";
import {FormattedMessage} from "react-intl";

class EventCalendar extends Component {
    componentDidMount(){
        this.props.fetchCalendarEvents(this.props.match.params.id);
        this.props.fetchSingleEvent(this.props.match.params.id);
    };

    render() {
        const calendarEvents = this.props.calendarEvents.map(e =>{
            return {...e,title: "#" + e.number + " "+ e.groupTitle}
        });

        return (
            <Fragment>
                {this.props.event && <GroupControlPanel/>}

                {
                    this.props.calendarEvents.length > 0 ?
                        <Calendar events={calendarEvents}/>
                        :
                        <div>
                            <FormattedMessage id="sorry"/>
                        </div>
                }
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
  calendarEvents: state.events.calendarEvents,
    event: state.events.singleEvent,
    user: state.users.user
});
const mapDispatchToProps = dispatch =>({
    fetchCalendarEvents: (id) => dispatch(fetchCalendarEvents(id)),
    fetchSingleEvent: (id) => dispatch(fetchSingleEvent(id)),
});
export default connect(mapStateToProps,mapDispatchToProps)(EventCalendar);