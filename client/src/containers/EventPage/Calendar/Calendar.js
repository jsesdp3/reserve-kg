import React, {Fragment} from 'react';
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import ruLocale from '@fullcalendar/core/locales/ru';
import moment from 'moment';
import Container from "reactstrap/es/Container";

const Calendar = (props) => {

    const timeFormat ={
        hour: 'numeric',
        minute: '2-digit',
        meridiem: false,
        hour12: false
    };

    return (
        <Fragment>
            <Container>
            <FullCalendar defaultView="dayGridMonth" plugins={[ dayGridPlugin ]}
                          firstDay={1}
                          eventTimeFormat={timeFormat}
                          defaultDate={props.events[0].start}
                          events={props.events}
                          locale={ruLocale}
                          eventClick={props.onEventClick}
                          eventRender={(info) => {
                              info.el.setAttribute('data-date', moment(info.event.start).format('YYYY-MM-DD'))
                          }}
                          />
            </Container>
        </Fragment>
    );
};

export default Calendar;