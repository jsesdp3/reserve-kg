import React, {Component, Fragment} from 'react';
import {fetchOrganization, fetchOrganizationEvents} from "../../store/actions/organizationsActions";
import {connect} from "react-redux";
import config from "../../config";
import {Col, Container, Row} from "reactstrap";
import {FormattedMessage} from "react-intl";
import Slider from "react-slick";
import {Map, Marker, Popup, TileLayer} from "react-leaflet";
import "./AllOrganizations.css"
import EventCard from "../../components/EventCard/EventCard";

import "./SlickCarousel.css";
import "./Organization.css";

class Organization extends Component {
    componentDidMount() {
        this.props.fetchOrganization(this.props.match.params.id);
        this.props.fetchOrganizationEvents(this.props.match.params.id);
    }

    render() {
        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1
        };

        if (!this.props.organization) {
            return null;
        }
        const location = this.props.organization.location.coordinates.reverse();

        return (
            <Fragment>
                <Container fluid>
                    <div className="Organization-info">
                        <img className="Organization-Logo"
                                 src={config.apiURL + '/uploads/thumb/' + this.props.organization.logo}
                                 alt="organization logo"
                            />

                        <div className="Organization-rightinfo">
                            <h2 className="Organization-Title">{this.props.organization.name}</h2>
                            <p className="Organization-Description">
                                <strong><FormattedMessage id="aboutUs"/>: </strong>
                                {this.props.organization.description}
                            </p>
                            <p>
                                <strong><FormattedMessage id="address"/> </strong>
                                {this.props.organization.address}
                            </p>
                        </div>
                    </div>
                    <div className="gallery-slider">
                        <Slider {...settings}>
                            {
                                this.props.organization.gallery.map(slider => (
                                    <img key={slider} className='Slide-img' src={config.apiURL + '/uploads/' + slider} alt={this.props.organization.name}/>
                                ))
                            }
                        </Slider>
                    </div>
                    <Map className="EventPage-Map" center={location} zoom={16}>
                        <TileLayer attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                   url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                        />
                        <Marker position={location}>
                            <Popup>
                                {this.props.organization.address}
                            </Popup>
                        </Marker>
                    </Map>

                    <hr/>
                    <h2><FormattedMessage id="titleEvents"/></h2>

                    <Row>
                        <Col sm={12} className="Events-cards">
                            {this.props.events.map(event => (
                                <EventCard
                                    key={event._id}
                                    event={event}
                                    user={this.props.user}
                                />
                            ))}
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    organization: state.organizations.organization,
    events: state.organizations.organizationEvents,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchOrganization: id => dispatch(fetchOrganization(id)),
    fetchOrganizationEvents: id => dispatch(fetchOrganizationEvents(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Organization);