import React, {Component} from 'react';
import {FormattedMessage, injectIntl} from "react-intl";
import {connect} from "react-redux";
import {fetchAllOrganizations} from "../../store/actions/organizationsActions";
import config from "../../config";
import {Link} from "react-router-dom";
import {Button, Card, CardBody, Container} from "reactstrap";
import "./AllOrganizations.css"


class AllOrganizations extends Component {

    componentDidMount() {
        this.props.fetchAllOrganizations();
    };

    render() {
        return (
            <Container fluid>
                <div className="Organizations-cards">
                    {this.props.organizations.length > 0 ? this.props.organizations.map(organization => (
                        <Card key={organization._id}>
                            <CardBody>
                                <h2 className="Organization-Title">{organization.name}</h2>
                                <img className="Organization-Logo" src={config.apiURL + '/uploads/thumb/' + organization.logo} alt="organization logo" />
                                <Link to={"/organizations/" + organization._id} className="Events-Watch-Event-Button">
                                    <Button color="success">
                                        <FormattedMessage id="viewOrganization"/>
                                    </Button>
                                </Link>
                            </CardBody>
                        </Card>
                    )) : (
                        <div>
                            Here will be organizations
                        </div>
                    )}
                </div>
            </Container>
        );
    }
}


const mapStateToProps = state => ({
    organizations: state.organizations.organizations
});

const mapDispatchToProps = dispatch =>({
    fetchAllOrganizations: () => dispatch(fetchAllOrganizations())
});

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(AllOrganizations));