import React, {Component, Fragment} from 'react';
import {FormattedMessage, injectIntl} from "react-intl";
import {Col, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {Map, Marker, TileLayer} from "react-leaflet";

class OrganizationForm extends Component {
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    render() {


        return (
            <Fragment>
                <FormElement
                    propertyName="name"
                    title={<FormattedMessage id="organizationName"/>}
                    type="text"
                    value={this.props.formConfig.name.value}
                    onChange={this.props.formConfig.name.onChange}
                    placeholder={this.props.intl.formatMessage({id: 'enterOrganizationName'})}
                    required
                />
                <FormElement
                    propertyName="description"
                    title={<FormattedMessage id="organizationDescription"/>}
                    type="text"
                    value={this.props.formConfig.description.value}
                    onChange={this.props.formConfig.description.onChange}
                    placeholder={this.props.intl.formatMessage({id: 'enterOrganizationDescription'})}
                    required
                />
                <FormElement
                    propertyName="address"
                    title={<FormattedMessage id="address"/>}
                    type="text"
                    value={this.props.formConfig.address.value}
                    onChange={this.props.formConfig.address.onChange}
                    required
                />

                <FormElement
                    propertyName="logo"
                    title={<FormattedMessage id="organizationLogo"/>}
                    type="file"
                    onChange={this.props.formConfig.logo.onChange}
                />

                <FormGroup row>
                    <Label for="gallery" sm={2}>Галерея</Label>
                    <Col sm={10}>
                        <Input
                            type="file"
                            name="gallery" id="gallery"
                            onChange={this.props.formConfig.gallery.onChange}
                            multiple
                        />
                    </Col>
                </FormGroup>


                <FormGroup>
                    <Map className="EventForm-Map" center={this.props.formConfig.location.center}
                         zoom={16} onClick={this.props.formConfig.location.onClick}>
                        <TileLayer
                            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                        />
                        {this.props.formConfig.location.markerLocation && <Marker position={this.props.formConfig.location.markerLocation}/>}
                    </Map>
                </FormGroup>

            </Fragment>
        );
    }
}

export default injectIntl(OrganizationForm);