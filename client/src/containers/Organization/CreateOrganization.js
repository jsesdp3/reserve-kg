import React, {Component, Fragment} from 'react';
import {Button, Col, Container, Form, FormGroup} from "reactstrap";
import {FormattedMessage} from "react-intl";
import {connect} from "react-redux";
import {createOrganization} from "../../store/actions/organizationsActions";
import OrganizationForm from "./OrganizationForm";

class CreateOrganization extends Component {
    state={
      name:'',
      description:'',
      address:'',
      location: '',
      logo:'',
      gallery: [],
      markerLocation: null,
      center: [42.87617,74.614606],
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    galleryChangeHandler = event => {
        event.preventDefault();

        let files = Array.from(event.target.files);

        files.forEach((file) => {
            let reader = new FileReader();
            reader.onloadend = () => {
                this.setState({
                    gallery: [...this.state.gallery, file],
                });
            };
            reader.readAsDataURL(file);
        });
    };

    mapClickHandler= event => {
        this.setState({
            markerLocation: event.latlng,
            location: event.latlng.lat + "," + event.latlng.lng
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();

        for (let i = 0; i < this.state.gallery.length; i++) {
            formData.append('gallery', this.state.gallery[i]);
        }

        Object.keys(this.state).forEach(key => {
            if (!['center', 'markerLocation', 'gallery'].includes(key)) {
                formData.append(key, this.state[key]);
            }
        });

        this.props.createOrganization(formData);
    };

    render() {

        const formConfig = {
            name: {
                value: this.state.name,
                onChange: this.inputChangeHandler
            },
            description: {
                value: this.state.description,
                onChange: this.inputChangeHandler
            },
            address: {
                value: this.state.address,
                onChange: this.inputChangeHandler
            },
            logo: {
                onChange: this.fileChangeHandler
            },
            location: {
                center: this.state.center,
                markerLocation: this.state.markerLocation,
                onClick: this.mapClickHandler
            },
            gallery:{
                onChange: this.galleryChangeHandler
            }
        };

        return (
        <Fragment>
            <Container>

                <h2><FormattedMessage id="createOrganization"/></h2>
                <Form onSubmit={this.submitFormHandler}>
                    <OrganizationForm formConfig={formConfig}/>
                    <FormGroup>
                        <Col>
                            <Button type="submit" color="primary"><FormattedMessage id="add"/></Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Container>
        </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch =>({
    createOrganization: organizationData => dispatch(createOrganization(organizationData))
});

export default connect (null,mapDispatchToProps) (CreateOrganization);