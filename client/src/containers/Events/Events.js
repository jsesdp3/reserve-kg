import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import 'moment/locale/ru';
import {Link, NavLink} from "react-router-dom";
import {FormattedMessage, intlShape, injectIntl } from "react-intl";

import {
    Col, CustomInput, FormGroup,
    ListGroup,
    ListGroupItem,
    Row,
} from "reactstrap";
import {fetchEvents} from "../../store/actions/eventsActions";

import "./Events.css"
import {fetchCategories} from "../../store/actions/categoriesActions";
import EventCard from "../../components/EventCard/EventCard";


class Events extends Component {

    state = {
        lng: '',
        lat: ''
    };

    componentDidMount() {
        this.props.onFetchEvents();
        this.props.fetchCategories()
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.match.params.id !== prevProps.match.params.id || this.state.lng !== prevState.lng || this.state.lat !== prevState.lat) {
                this.props.onFetchEvents(this.props.match.params.id, this.state.lng, this.state.lat);
        }
     }

     getLocation = (event) => {
         if (event.checked) {
             if (navigator.geolocation) {
                 navigator.geolocation.getCurrentPosition(this.showPosition)
             }
         } else {
             this.setState({
                 lng: '',
                 lat: ''
             })
         }
    };

     showPosition = (position) => {
         this.setState({
             lng: position.coords.longitude,
             lat: position.coords.latitude
         })
    };

    render() {
        const intl = this.props.intl;
        return (
            <div className="main-event-block">
                <Fragment>
                    {!this.props.user || this.props.user.verification === true ?
                    <Row className="main-block">
                        <Col sm={12} className="Events-Col">
                            <FormattedMessage id="titleEvents" tagName="h1"/>
                            <FormGroup>
                                <div>
                                    <CustomInput onClick={(e) => this.getLocation(e.target)} type="checkbox" id="exampleCustomCheckbox" label={<FormattedMessage id="closerToMe"/>} />
                                </div>
                            </FormGroup>
                        </Col>
                        <Col sm={2}>
                            <ListGroup>
                                <ListGroupItem tag={NavLink} to="/" exact><FormattedMessage
                                    id="allCategories"/></ListGroupItem>
                                {this.props.categories.map(category => (
                                   <Fragment key={category._id}>
                                       {
                                           intl.locale === 'ru' ?
                                               <ListGroupItem
                                                    tag={NavLink}
                                                   to={"/category/" + category._id}>{category.titleRu}
                                               </ListGroupItem> :
                                               intl.locale === 'en' ?
                                                   <ListGroupItem
                                                       tag={NavLink}
                                                       to={"/category/" + category._id}>{category.titleEn}
                                                   </ListGroupItem> :
                                                   <ListGroupItem
                                                       tag={NavLink}
                                                       to={"/category/" + category._id}>{category.titleKy}
                                                   </ListGroupItem>
                                       }
                                   </Fragment>

                                ))}
                            </ListGroup>
                        </Col>
                        <Col sm={10} className="Events-cards">
                            {this.props.events.map(event => (
                                <EventCard
                                    key={event._id}
                                    event={event}
                                    user={this.props.user}
                                />
                            ))}
                        </Col>
                    </Row>
                    :<div className="Events-verification">
                        <h5>Здравствуйте, <strong>{this.props.user.displayName}</strong></h5>
                        <p>Для дальнейшего использования нашего сервиса вам необходимо подтвердить регистрацию</p>
                        <Link to={`/verification/${this.props.user._id}`}>Подтвердить регистрацию</Link>
                    </div>
                    }
                </Fragment>
            </div>
        );
    }
}

Events.propTypes = {
    intl: intlShape.isRequired
};

const mapStateToProps = state => ({
    events: state.events.events,
    user: state.users.user,
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
    onFetchEvents: (id,lng,lat) => dispatch(fetchEvents(id,lng,lat)),
    fetchCategories: () => dispatch(fetchCategories()),
});

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Events));