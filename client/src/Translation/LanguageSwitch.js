import React from "react";
import { IntlContext } from "./IntlContext";
import {Button} from "reactstrap";

const LanguageSwitch = () => {
    const { switchToEnglish, switchToRussian, switchToKyrgyz } = React.useContext(IntlContext);
    return (
        <>
            <Button color='link' onClick={switchToEnglish}>En</Button>
            <Button color='link' onClick={switchToRussian}>Руc</Button>
            <Button color='link' onClick={switchToKyrgyz}>Кырг</Button>
        </>
    );
};

// ... OR:
//
// const LanguageSwitch = () => (
//   <IntlContext.Consumer>
//     {({ switchToEnglish, switchToDeutsch }) => (
//       <React.Fragment>
//         <button onClick={switchToEnglish}>
//           English
//         </button>
//         <button onClick={switchToDeutsch}>
//           Deutsch
//         </button>
//       </React.Fragment>
//     )}
//   </IntlContext.Consumer>
// );

export default LanguageSwitch;
