import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Events from "./containers/Events/Events";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import NewEvent from "./containers/NewEvent/NewEvent";
import GroupPage from "./containers/GroupPage/GroupPage";
import NewGroup from "./containers/NewGroup/NewGroup";
import EventPage from "./containers/EventPage/EventPage"
import EventCalendar from "./containers/EventPage/Calendar/EventCalendar";
import UserCalendar from "./containers/EventPage/Calendar/UserCalendar";
import AdminPanel from "./containers/Admin/AdminPanel";
import EditEvent from "./containers/EditEvent/EditEvent";
import CreateCategoryForm from "./containers/CreateCategoryForm/CreateCategoryForm";
import CategoryManagement from "./containers/CategoryManagement/CategoryManagement";
import Organization from "./containers/Organization/Organization";
import Verification from "./containers/Verificarion/Verification";
import CreateOrganization from "./containers/Organization/CreateOrganization";
import AllOrganizations from "./containers/Organization/AllOrganizations";

const ProtectedRoute = ({isAllowed, ...props}) => {
  return isAllowed ? <Route {...props} /> : <Redirect to="/login" />
};

const Routes = ({user,event}) => {
  return (
    <Switch>

      <Route path="/" exact component={Events}/>
      <Route path="/category/:id" exact component={Events}/>

        <ProtectedRoute
            isAllowed={(user && user.role === 'admin') || (user && user.role === 'user')}
            path="/events/new"
            exact component={NewEvent}
          />
                <Route path="/register" exact component={Register}/>
                <Route path="/login" exact component={Login}/>
                <Route path="/my_calendar" exact component={UserCalendar}/>
                <Route path="/organizations" exact component={AllOrganizations}/>
                <Route path="/organizations/:id" exact component={Organization}/>
                <Route path="/create_organization" exact component={CreateOrganization}/>
                <Route path="/events/:id/calendar" exact component={EventCalendar}/>
                <Route path="/events/:id" exact component={EventPage}/>
                <Route path="/events/:id/edit" exact component={EditEvent}/>
                <Route path="/events/:id/groups" exact component={GroupPage}/>
                <Route path="/events/:id/groups/add" exact component={NewGroup}/>
                <Route path="/new_category" exact component={CreateCategoryForm}/>
                <Route path="/verification/:id" exact component={Verification}/>

          <ProtectedRoute
            isAllowed={(user && user.role === 'admin')}
            path="/admin/users"
            exact component={AdminPanel}
          />
          <ProtectedRoute
            isAllowed={(user && user.role === 'admin')}
            path={"/categories/management"}
            exact component={CategoryManagement}
          />
    </Switch>

  );
};

export default Routes;