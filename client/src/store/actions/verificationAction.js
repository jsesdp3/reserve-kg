import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";
import {FormattedMessage} from "react-intl";
import React from "react";
import {push} from "connected-react-router";
export const FETCH_VERIFICATION_SUCCESS = 'FETCH_VERIFICATION_SUCCESS';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';

export const fetchVerificationSuccess = (verification) => ({type: FETCH_VERIFICATION_SUCCESS, verification});
export const fetchUserSuccess = (userVer) => ({type: FETCH_USER_SUCCESS, userVer});

export const postVerificationSms = (data, id) => {
    return dispatch => {
        axios.post(`/verification/${id}`, data).then(
            response => {
                dispatch(fetchVerificationSuccess(response.data));
                NotificationManager.success("Код запрошен!");
            }
        )
    }
};

export const putCodeToChangeVerification = (data, id) => {
    return dispatch => {
        axios.put(`/verification/${id}`, data).then(response => {
            dispatch(fetchVerificationSuccess(response.data));
        })
    }
};

export const getVerificationUser = (id) => {
    return dispatch => {
        axios.get(`/verification/${id}`).then(response => {
            dispatch(fetchUserSuccess(response.data));

            if (response.data.verification === true) {
                    NotificationManager.success(<FormattedMessage id="successfullyRegistered"/>);
                    dispatch(push(`/`));
                }
        })
    }
};
