import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';
import axios from '../../axios-api';
import {FormattedMessage} from "react-intl";
import React from "react";

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGOUT_USER = 'LOGOUT_USER';

const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const logoutUser = () => {
  return (dispatch) => {
    return axios.delete('/users/sessions').then(
      () => {
        dispatch({type: LOGOUT_USER});
        NotificationManager.success(<FormattedMessage id="youAreLoggedOut"/>);
        dispatch(push('/login'));
      },
      error => {
        NotificationManager.error(<FormattedMessage id="couldNotLogOut"/>);
      }
    )
  }
};

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      response => {
        dispatch(registerUserSuccess(response.data.user));
        dispatch(push(`/verification/${response.data.user._id}`));
      },
      error => {
        if (error.response) {
          dispatch(registerUserFailure(error.response.data));
        } else {
          dispatch(registerUserFailure({global: <FormattedMessage id="noConnection"/>}));
        }
      }
    )
  }
};

export const loginUser = userData => {
  return (dispatch) => {
    return axios.post('/users/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data.user));
        NotificationManager.success(<FormattedMessage id="youHaveSuccessfullyLoggedIn"/>);
        dispatch(push('/'));
      },
      error => {
        if (error.response) {
          dispatch(loginUserFailure(error.response.data));
        } else {
          dispatch(loginUserFailure({global: <FormattedMessage id="noConnection"/>}));
        }
      }
    )
  }
};

export const facebookLogin = userData => {
  return dispatch => {
    return axios.post('/users/facebookLogin', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data.user));
        NotificationManager.success(<FormattedMessage id="youHaveSuccessfullyLoggedInViaFacebook"/>);
        dispatch(push('/'));
      },
      () => {
        dispatch(loginUserFailure(<FormattedMessage id="couldNotLogInWithFacebook"/>));
      }
    )
  }
};


export const googleLogin = userData => {
    console.log(userData, 'response data');
    return dispatch => {
        return axios.post('/users/googleLogin', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                NotificationManager.success(<FormattedMessage id="youHaveSuccessfullyLoggedInViaGoogle"/>);
                dispatch(push('/'));
            },
            () => {
                dispatch(loginUserFailure(<FormattedMessage id="couldNotLogInWithGoogle"/>));
            }
        )
    }
};
