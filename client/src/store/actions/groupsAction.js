import axios from '../../axios-api';


export const FETCH_GROUPS_SUCCESS = 'FETCH_GROUPS_SUCCESS';

export const fetchGroupsSuccess = groups => ({type: FETCH_GROUPS_SUCCESS, groups});

export const fetchGroups = () => {
    return dispatch => {
        return axios.get('/groups').then(
            response => dispatch(fetchGroupsSuccess(response.data))
        );
    };
};