import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";
import {FormattedMessage} from "react-intl";
import React from "react";

export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const PATCH_USER_SUCCESS = 'PATCH_USER_SUCCESS';

export const fetchUsersSuccess = adminUsers => ({type: FETCH_USERS_SUCCESS, adminUsers});
export const patchUserSuccess = user => ({type: PATCH_USER_SUCCESS, user});

export const fetchAdminUsers = () => {
  return dispatch => {
    return axios.get('/admin/users').then(
      response => {
        dispatch(fetchUsersSuccess(response.data));
      }
    );
  };
};

export const patchUser = (id, role) => {
  return (dispatch) => {
    return axios.patch(`/admin/users/role_change/${id}`, {id, role: role}).then(
      () => {
        dispatch(patchUserSuccess());
        dispatch(fetchAdminUsers());
        NotificationManager.success(<FormattedMessage id="roleChanged"/>);
      }
    );
  };
};