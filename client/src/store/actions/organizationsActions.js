import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager} from "react-notifications";
import {FormattedMessage} from "react-intl";
import React from "react";


export const CREATE_ORGANIZATION_SUCCESS = 'CREATE_ORGANIZATION_SUCCESS';
export const FETCH_ORGANIZATION_SUCCESS = 'FETCH_ORGANIZATION_SUCCESS';
export const FETCH_ALL_ORGANIZATIONS_SUCCESS = "FETCH_ALL_ORGANIZATIONS_SUCCESS";
export const FETCH_ORGANIZATION_EVENTS_SUCCESS = 'FETCH_ORGANIZATION_EVENTS_SUCCESS';

export const createOrganizationSuccess = organization => ({type: CREATE_ORGANIZATION_SUCCESS, organization});
export const fetchOrganizationSuccess = organization => ({type: FETCH_ORGANIZATION_SUCCESS, organization});
export const fetchAllOrganizationsSuccess = organizations => ({type:FETCH_ALL_ORGANIZATIONS_SUCCESS,organizations});
export const fetchOrganizationEventsSuccess = events => ({type:FETCH_ORGANIZATION_EVENTS_SUCCESS,events});

export const createOrganization = (organizationData, redirect = true) => {
    return dispatch => {
        return axios.post('/organizations/', organizationData).then(
            response => {
                dispatch(createOrganizationSuccess(response.data));
                NotificationManager.success(<FormattedMessage id="youHaveSuccessfullyCreatedAnOrganization"/>);
                if (redirect) {
                    dispatch(push('/organizations/' + response.data._id));
                }
            },
            error => {

            }
        )
    }
};

export const fetchOrganization = id => {
    return dispatch => {
        return axios.get('/organizations/' + id).then(
            response => {
                dispatch(fetchOrganizationSuccess(response.data));
            }
        )
    }
};

export const fetchAllOrganizations = () =>{
    return dispatch =>{
        return axios.get('/organizations/').then(
            response => dispatch (fetchAllOrganizationsSuccess(response.data))
        );
    };
};

export const fetchOrganizationEvents = id => {
    return dispatch => {
        return axios.get(`/events/by_organization/${id}`).then(
            response => dispatch(fetchOrganizationEventsSuccess(response.data))
        );
    };
};