import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";
import {FormattedMessage} from "react-intl";
import React from "react";
import {push} from "connected-react-router";
export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';

export const fetchCategoriesSuccess = (categories) => ({type: FETCH_CATEGORIES_SUCCESS, categories});

export const fetchCategories = () => {
  return dispatch => {
      return axios.get('/categories').then(response => {
          dispatch(fetchCategoriesSuccess(response.data))
      })
  }
};

export const createCategories = (data) => {
    return dispatch => {
        axios.post('/categories', data).then(
            () => {
                NotificationManager.success(<FormattedMessage id="categoryAddedSuccessfully"/>);
                dispatch(fetchCategories());
                dispatch(push(`/categories/management`));
            }
        )
    }
};


export const deleteCategory = (categoryId) => {
  return dispatch => {
      return axios.delete('/categories/' + categoryId).then(
          () => {
              NotificationManager.success(<FormattedMessage id="categorySuccessfullyDeleted"/>);
              dispatch(fetchCategories())
          },
          error => {
              if (error && error.response) {
                  NotificationManager.error(error.response.data.message);
              }
          }
      )
  }
};