import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager} from "react-notifications";
import {FormattedMessage} from "react-intl";
import React from "react";

export const FETCH_EVENTS_SUCCESS = 'FETCH_EVENTS_SUCCESS';
export const CREATE_EVENT_SUCCESS = 'CREATE_EVENT_SUCCESS';
export const PATCH_EVENT_SUCCESS = 'PATCH_EVENT_SUCCESS';
export const EVENT_MODERATION_SUCCESS = 'EVENT_MODERATION_SUCCESS';
export const FETCH_SINGLE_EVENT_SUCCESS ='FETCH_SINGLE_EVENT_SUCCESS';
export const FETCH_CALENDAR_EVENTS_SUCCESS = "FETCH_CALENDAR_EVENTS_SUCCESS";

export const FETCH_USER_EVENTS_SUCCESS = 'FETCH_USER_EVENTS_SUCCESS';

export const fetchEventsSuccess = events => ({type: FETCH_EVENTS_SUCCESS, events});
export const patchEventSuccess = events => ({type: PATCH_EVENT_SUCCESS, events});
export const createEventsSuccess = events =>({type: CREATE_EVENT_SUCCESS,events});
export const fetchSingleEventSuccess = (event,newEvent) =>({type: FETCH_SINGLE_EVENT_SUCCESS,event,newEvent});
export const eventModerationSuccess = () => ({type: EVENT_MODERATION_SUCCESS});
export const fetchCalendarEventsSuccess =(calendarEvents) =>({type:FETCH_CALENDAR_EVENTS_SUCCESS,calendarEvents});

export const fetchUserEventsSuccess = events =>({type:FETCH_USER_EVENTS_SUCCESS,events});

export const fetchEvents = (id, lng, lat) => {
  return dispatch => {
      let url = "/events";

      if (id && !lng && !lat) {
          url += `?category=${id}`
      } else if (!id && lng && lat) {
          url += `?lng=${lng}&lat=${lat}`
      } else if (id && lng && lat) {
          url += `?lng=${lng}&lat=${lat}&category=${id}`
      }
      return axios.get(url).then(
      response => {
          dispatch(fetchEventsSuccess(response.data))
      }
    );
  };
};

export const fetchSingleEvent =(id)=>{
  return dispatch =>{
    return axios.get(`/events/${id}`).then(
        response => dispatch(fetchSingleEventSuccess(response.data))
    )
  };
};

export const fetchGroupsOnEvent =(id)=>{
    return dispatch =>{
        return axios.get(`/events/${id}/groups`).then(
            response => dispatch(fetchSingleEventSuccess(response.data))
        )
    };
};

export const fetchCalendarEvents = (id) =>{
    return dispatch =>{
        return axios.get(`/calendar/event/${id}`).then(
            response => dispatch (fetchCalendarEventsSuccess(response.data))
        )
    }
};

export const fetchUserEvents = () =>{
  return dispatch =>{
    return axios.get('/calendar/my').then(
        response =>dispatch(fetchUserEventsSuccess(response.data))
    )
  };
};

export const createEvent = eventData =>{
    return dispatch =>{
    return axios.post('/events',eventData).then(()=>dispatch(createEventsSuccess()));
  }
};

export const editEvent = (eventId, eventData) => {
    return dispatch => {
        axios.put(`/events/${eventId}`, eventData).then(() => {
            dispatch(push(`/events/${eventId}`));
            NotificationManager.success(<FormattedMessage id="courseChanged"/>);
        })
    }
};

export const patchEvent = (id, status) => {
  return (dispatch) => {
    return axios.patch('/events/'+id, {id, status}).then(
        () => {
          dispatch(patchEventSuccess());
          dispatch(fetchEvents());
          NotificationManager.success(<FormattedMessage id="statusChanged"/>);
        }
    );
  };
};

export const eventModeration = (id, status) => {
    return (dispatch) => {
        axios.patch(`/events/moderation_events/${id}`,{status}).then(
           response => {

               dispatch(eventModerationSuccess());
               dispatch(fetchEvents());
               NotificationManager.success(<FormattedMessage id="youHaveSuccessfullySentForModeration"/>)
           }
       )
   }
};

export const  subscribeToEvent = (id, eventId) =>{
    console.log(eventId);
    return dispatch => {
        axios.patch(`/events/submit/${id}`).then(
            () => {
                dispatch(patchEventSuccess());
                dispatch(fetchSingleEvent(eventId));
                NotificationManager.success(<FormattedMessage id="youHaveSuccessfullySignedUpForTheCourse"/>);
            },
            error => {
                if (error.response) {
                    NotificationManager.warning(<FormattedMessage id="selectGroupToJoin"/>);
                }
            }
        )
    }
};

export const createGroup = (id, eventData) => {
    return dispatch => {
        axios.post(`/events/${id}/groups`, eventData).then(() => {
            NotificationManager.success(<FormattedMessage id="groupCreated"/>);
            dispatch(push(`/events/${id}/groups`));
        })
    }
};

export const deleteGroup = (group, eventId) => {
    return (dispatch) => {
        const groupId = group._id;
        return axios.delete(`/groups/${groupId}`).then(() => {
            NotificationManager.success(<FormattedMessage id="groupDeleted"/>);
            dispatch(fetchGroupsOnEvent(eventId));
        })
    }
};

export const deleteEvent = (eventId) => {
    return (dispatch) => {
        return axios.delete(`/events/${eventId}`).then(
            () => {
                NotificationManager.success(<FormattedMessage id="courseSuccessfullyDeleted"/>);
                dispatch(fetchEvents(eventId))
            }
        )
    }
};