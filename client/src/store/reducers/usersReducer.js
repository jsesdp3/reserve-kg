import {
  LOGIN_USER_FAILURE,
  LOGIN_USER_SUCCESS, LOGOUT_USER,
  REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS
} from "../actions/usersActions";
import {CREATE_ORGANIZATION_SUCCESS} from "../actions/organizationsActions";
import {FETCH_USER_SUCCESS} from "../actions/verificationAction";

const initialState = {
  registerError: null,
  loginError: null,
  user: null
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_USER_SUCCESS:
      return {...state, registerError: null, user: action.user};
    case REGISTER_USER_FAILURE:
      return {...state, registerError: action.error};
    case LOGIN_USER_SUCCESS:
      return {...state, user: action.user, loginError: null};
    case FETCH_USER_SUCCESS:
      return {...state, user: action.userVer, loginError: null};
    case LOGIN_USER_FAILURE:
      return {...state, loginError: action.error};
    case LOGOUT_USER:
      return {...state, user: null};
    case CREATE_ORGANIZATION_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          organization: action.organization
        }
      };
    default:
      return state;
  }
};

export default usersReducer;
