import {FETCH_VERIFICATION_SUCCESS} from "../actions/verificationAction";

const initialState = {
    verification: null,
};

const verificationReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_VERIFICATION_SUCCESS:
            return {...state, verification: action.verification};
        default:
            return state;
    }
};

export default verificationReducer;