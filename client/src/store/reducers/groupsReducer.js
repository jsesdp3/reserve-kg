import {FETCH_GROUPS_SUCCESS,} from "../actions/groupsAction";

const initialState = {
    groups: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GROUPS_SUCCESS:
            return {...state, groups: action.groups};
        default:
            return state;
    }
};

export default reducer;