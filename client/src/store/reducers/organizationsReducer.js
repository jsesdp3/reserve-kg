import {
    FETCH_ORGANIZATION_SUCCESS,
    FETCH_ALL_ORGANIZATIONS_SUCCESS,
    FETCH_ORGANIZATION_EVENTS_SUCCESS
} from "../actions/organizationsActions";

const initialState = {
    organization: null,
    organizations: [],
    organizationEvents: []
};

const organizationsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ORGANIZATION_SUCCESS:
            return {
                ...state,
                organization: action.organization
            };
        case FETCH_ALL_ORGANIZATIONS_SUCCESS:
            return {
                ...state,
                organizations: action.organizations
            };
        case FETCH_ORGANIZATION_EVENTS_SUCCESS:
            return {
                ...state,
                organizationEvents: action.events
            };
        default:
            return state;
    }
};

export default  organizationsReducer;