import {
  FETCH_CALENDAR_EVENTS_SUCCESS,
  FETCH_EVENTS_SUCCESS,
  FETCH_SINGLE_EVENT_SUCCESS, FETCH_USER_EVENTS_SUCCESS
} from "../actions/eventsActions";

const initialState = {
  events: [],
  calendarEvents: [],
  userEvents:[],
  singleEvent: null,
  singleNewEvent: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_EVENTS_SUCCESS:
      return {...state, events: action.events};
    case FETCH_SINGLE_EVENT_SUCCESS:
      return {...state,singleEvent: action.event,singleNewEvent:action.newEvent};
    case FETCH_CALENDAR_EVENTS_SUCCESS:
      return {...state,calendarEvents: action.calendarEvents};
    case FETCH_USER_EVENTS_SUCCESS:
      return {...state,userEvents: action.events};
    default:
      return state;
  }
};

export default reducer;