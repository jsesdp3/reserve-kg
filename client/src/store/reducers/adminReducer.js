import {
  FETCH_USERS_SUCCESS
} from "../actions/adminActions";

const initialState = {
  adminUsers: []
};

const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS_SUCCESS:
      return {...state, adminUsers: action.adminUsers};
    default:
      return state;
  }
};

export default adminReducer;
