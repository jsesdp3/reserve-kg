import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router';
import * as serviceWorker from './serviceWorker';

import store, {history} from './store/configureStore';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications/lib/notifications.css';

import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';

import './custom.css';
import {IntlProviderWrapper} from "./Translation/IntlContext";




const app = (
    <IntlProviderWrapper>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                    <App/>
            </ConnectedRouter>
        </Provider>
    </IntlProviderWrapper>
);

ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();
