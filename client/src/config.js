const config = {
    apiURL: 'http://localhost:8000',
    siteURL: 'https://reserve.net.kg'
};

switch(process.env.REACT_APP_ENV) {
    case 'test':
        config.apiURL = 'http://localhost:8010';
        break;
    case 'production':
        config.apiURL = 'https://reserve.net.kg:8000';
        break;
    default:
}

export default config;
