import React, {Component} from 'react';
import {Button, Col, CustomInput, Form, FormGroup, Label} from "reactstrap";
import FormElement from "../UI/Form/FormElement";

import './GroupForm.css';
import TimePicker from "react-time-picker";
import {FormattedMessage} from "react-intl";

class GroupForm extends Component {
    state = {
        name: '',
        weekDays: [],
        time: '',
        event: this.props.id
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    inputChangeCheckboxHandler = event => {
        const weekDays = this.state.weekDays;
        let index;

        if (event.target.checked) {
            weekDays.push(event.target.value)
        } else {
            index = weekDays.indexOf(event.target.value);
            weekDays.splice(index, 1)
        }

        this.setState({ weekDays: weekDays });
    };

    onChange = time => this.setState({ time });

    submitFormHandler = event => {
        event.preventDefault();
        this.props.onSubmit(this.props.id, {...this.state});
    };

    render() {
        return (
            <div className="Group-form">
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="name"
                        title=<FormattedMessage id="groupName"/>
                        type="text"
                        value={this.state.name}
                        onChange={this.inputChangeHandler}
                        required
                    />
                    <FormGroup className="Week-Days-block">
                        <Label><FormattedMessage id="selectTheDaysOfTheWeek"/></Label>
                        <div className="Week-Days">
                            <CustomInput type="checkbox" onChange={this.inputChangeCheckboxHandler.bind(this)} id="first-day" value="1" label=<FormattedMessage id="monday"/> inline />
                            <CustomInput type="checkbox" onChange={this.inputChangeCheckboxHandler.bind(this)} id="second-day" value="2" label=<FormattedMessage id="tuesday"/> inline />
                            <CustomInput type="checkbox" onChange={this.inputChangeCheckboxHandler.bind(this)} id="third-day" value="3" label=<FormattedMessage id="wednesday"/> inline />
                            <CustomInput type="checkbox" onChange={this.inputChangeCheckboxHandler.bind(this)} id="fourth-day" value="4" label=<FormattedMessage id="thursday"/> inline />
                            <CustomInput type="checkbox" onChange={this.inputChangeCheckboxHandler.bind(this)} id="fifth-day" value="5" label=<FormattedMessage id="friday"/> inline />
                            <CustomInput type="checkbox" onChange={this.inputChangeCheckboxHandler.bind(this)} id="sixth-day" value="6" label=<FormattedMessage id="saturday"/> inline />
                            <CustomInput type="checkbox" onChange={this.inputChangeCheckboxHandler.bind(this)} id="seventh-day" value="7" label=<FormattedMessage id="sunday"/> inline />
                        </div>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="time" sm={2}><FormattedMessage id="occupationTime"/></Label>
                        <Col sm={10}>
                            <TimePicker
                                onChange={this.onChange}
                                value={this.state.time}
                                format="H:mm"
                                locale="ru-RU"
                                name="time"
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" className="Add-group"><FormattedMessage id="add"/></Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default GroupForm;