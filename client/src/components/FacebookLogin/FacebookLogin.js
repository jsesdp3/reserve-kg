import React, {Component} from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props'
import {NotificationManager} from 'react-notifications'
import {Button} from "reactstrap";
import {facebookLogin} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import {FormattedMessage} from "react-intl";


class FacebookLogin extends Component {

    facebookLogin = data => {
        if (data.error) {
            NotificationManager.error(<FormattedMessage id="somethingWentWrong"/>)
        } else if(!data.name){
            NotificationManager.warning(<FormattedMessage id="youClickedCancel"/>);
        } else {
            this.props.facebookLogin(data)
        }
    };
    render() {
        return (
            <FacebookLoginButton
                appId="390672758455990"
                callback={this.facebookLogin}
                fields="name, email, picture"
                render={renderProps => (
                    <Button outline color="primary" onClick={renderProps.onClick}><FormattedMessage id="loginWithFacebook"/></Button>
                )}
            />
        );
    }
}

const mapDispatchToProps = dispatch => ({
    facebookLogin: userData => dispatch(facebookLogin(userData))
});

export default connect(null, mapDispatchToProps)(FacebookLogin);


