import React from 'react';
import {
    Button,
    Card,
    CardBody,
    CardImg,
    CardText,
    CardTitle,
    Col,
    Row,
} from "reactstrap";
import config from "../../config";
import {FormattedMessage} from "react-intl";
import Moment from "react-moment";
import {Link} from "react-router-dom";
import EventDropdown from "../UI/EventDropdown"
const EventCard = ({event,user}) => {
    return (
        <div key={event._id} className="Events-Col-inside-map">
            <Card className="Events-Card">
                <CardBody>
                    <CardTitle>
                        <h2 className="Events-Title">{event.title}</h2>

                        <Col sm={6}>
                            {event.status === 'unpublished' ? <h5 className="Events-admin-text"><FormattedMessage id="notPublished"/></h5> : null}

                            {user && (user.role ==='user' || user.role ==='admin') && event.status === 'on_moderation' &&
                                <h5 className="Events-admin-text"> <FormattedMessage id="oNmoderation"/></h5>
                            }
                        </Col>

                        {(user && (user._id === event.user._id || user.role === 'admin')) &&
                            <EventDropdown event={event} user={user}/>
                        }

                    </CardTitle>
                    <Row>
                        <Col sm={12}>
                            <CardImg top className="Events-Image"
                                     src={config.apiURL + '/uploads/thumb/' + event.thumbImage}
                                     id="Event" alt="Event"
                            />
                        </Col>
                        <Col sm={10}>
                            <CardText className="Events-CardText">
                                <FormattedMessage id="courseStartDate"/> <Moment locale="ru" format="DD.MM.YYYY">{event.startDate}</Moment>
                            </CardText>
                            <CardText className="Events-CardText">
                                <FormattedMessage id="cost"/> {event.priceMonth}
                            </CardText>
                            <CardText className="Events-CardText">
                                <FormattedMessage id="numberOfSeats"/> {event.startPlaces}
                            </CardText>
                            {
                                event.dist && (
                                    <CardText className="Events-CardText">
                                        <FormattedMessage id="distance"/>: {parseInt(event.dist.calculated)} <FormattedMessage id="meters"/>
                                    </CardText>
                                )
                            }

                        </Col>
                    </Row>

                    <Link to={"/events/" + event._id} className="Events-Watch-Event-Button">
                        <Button color="success">
                            <FormattedMessage id="viewCourse"/>
                        </Button>
                    </Link>
                </CardBody>
            </Card>
        </div>
    );
};

export default EventCard;