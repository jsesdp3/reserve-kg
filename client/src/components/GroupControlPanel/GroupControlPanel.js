import React, {Component} from 'react';
import {Button, ButtonGroup} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import {connect} from "react-redux";

import "./GroupControlPanel.css";
import {FormattedMessage} from "react-intl";


class GroupControlPanel extends Component {
    render() {
        return this.props.user && (this.props.user._id === this.props.event.user || this.props.user.role === 'admin') &&
            <ButtonGroup className="GroupControlPanel" >
                <Button size="sm" color='primary' tag={RouterNavLink} exact to={`/events/${this.props.event._id}`}><FormattedMessage id="event"/></Button>
                <Button size="sm" color='primary' tag={RouterNavLink} exact to={`/events/${this.props.event._id}/groups/add`}><FormattedMessage id="addGroup"/></Button>
                <Button size="sm" color='primary' tag={RouterNavLink} exact to={`/events/${this.props.event._id}/groups`}><FormattedMessage id="viewGroups"/></Button>
                <Button size="sm" color='primary' tag={RouterNavLink} exact to={`/events/${this.props.event._id}/calendar`}><FormattedMessage id="viewSchedule"/></Button>
                <Button size="sm" color="primary" tag={RouterNavLink} exact to={`/events/${this.props.event._id}/edit`}><FormattedMessage id="edit"/></Button>
            </ButtonGroup>
    }
}
const mapStateToProps = state => ({
    event: state.events.singleEvent,
    user: state.users.user
});

export default connect(mapStateToProps)(GroupControlPanel);
