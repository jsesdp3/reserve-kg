import React, {Component,Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, InputGroup, InputGroupAddon, Label} from "reactstrap";
import {Map, Marker, TileLayer} from "react-leaflet"
import DatePicker from "react-date-picker";
import config from "../../config";

import FormElement from "../UI/Form/FormElement";

import 'leaflet/dist/leaflet.css';
import "./CreateEventForm.css";
import {connect} from "react-redux";
import {fetchCategories} from "../../store/actions/categoriesActions";
import {FormattedMessage, intlShape, injectIntl} from "react-intl";


class CreateEventForm extends Component {
    constructor(props) {
        super (props);

        if (this.props.event) {
            const markerLocation = [props.event.location.coordinates[1], props.event.location.coordinates[0]];

            this.state = {
                ...props.event,
                markerLocation: markerLocation,
                center: markerLocation,
                location: markerLocation.join(',')
            }
        } else {
            this.state={
                title: '',
                startDate: '',
                minPlaces:0,
                startPlaces:0,
                maxPlaces:0,
                address: '',
                location: '',
                markerLocation: null,
                center: [42.87617,74.614606],
                description:'',
                image:'',
                priceMonth:'',
                eventsNumber: '',
                category: ''
            };
        }
    }

    componentDidMount() {
        this.props.fetchCategories()
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    onChange = date => this.setState({ startDate: date });

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };
    mapClickHandler= event => {
        this.setState({
            markerLocation: event.latlng,
            location: event.latlng.lat + "," + event.latlng.lng
        });
    };

    submitFormHandler = event =>{
      event.preventDefault();
        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            if (!['center', 'markerLocation'].includes(key)) {
                formData.append(key, this.state[key]);
            }
        });
        console.log(formData);
        this.props.onSubmit(formData);
    };

    render() {
        const intl = this.props.intl;
        return (
            <Fragment>
                <Form onSubmit={this.submitFormHandler} id="create-event">
                    <FormElement
                        propertyName="title"
                        title={<FormattedMessage id="courseName"/>}
                        type="text"
                        value={this.state.title}
                        onChange={this.inputChangeHandler}
                        placeholder={this.props.intl.formatMessage({id: 'enterCourseName'})}
                        required

                    />

                    <FormElement
                        propertyName="category"
                        title={<FormattedMessage id="categories"/>}
                        type="select"
                        value={this.state.category}
                        onChange={this.inputChangeHandler}
                        required

                    >
                        <option value="">{this.props.intl.formatMessage({id: 'chooseCategory'})}</option>

                        {this.props.categories && this.props.categories.map(category => (
                            <Fragment key={category._id}>
                                {
                                    intl.locale === 'ru' ?
                                        <option value={category._id}>{category.titleRu}</option> :
                                            intl.locale === 'en' ?
                                                <option value={category._id}>{category.titleEn}</option> :
                                                    <option value={category._id}>{category.titleKy}</option>
                                }
                            </Fragment>

                        ))}
                    </FormElement>

                    <FormElement
                        propertyName="image"
                        title={<FormattedMessage id="poster"/>}
                        type="file"
                        onChange={this.fileChangeHandler}
                        required={!this.props.edit}
                    />

                    {
                        typeof this.state.image === "string" ?
                            <div className="Preview-image-block">
                                <img className="Preview-image" src={`${config.apiURL}/uploads/${this.state.image}`} alt={this.state.title}/>
                            </div> : null
                    }

                    <FormGroup row>
                        <Label for="startDate" sm={2}><FormattedMessage id="startDate"/></Label>
                        <DatePicker
                            className="Input-date"
                            onChange={this.onChange}
                            value={this.state.startDate}
                            name="startDate"
                            format="dd/MM/yyyy"
                            locale="ru-RU"
                            required
                        />
                    </FormGroup>

                    <FormElement
                        propertyName="eventsNumber"
                        title={<FormattedMessage id="numberOfClasses"/>}
                        type="number"
                        value={this.state.eventsNumber}
                        onChange={this.inputChangeHandler}
                        placeholder={this.props.intl.formatMessage({id: 'enterTheNumberClasses'})}
                        required min={0}

                    />

                    <FormGroup row>
                        <Label sm={2}><FormattedMessage id="numberOfSeats"/></Label>
                        <Col sm={10}>
                            <InputGroup>
                                <InputGroupAddon addonType="prepend"><FormattedMessage id="min"/></InputGroupAddon>
                                <Input type="number" min="0" name="minPlaces" id="minPlaces" placeholder="min"
                                       value={this.state.minPlaces} onChange={this.inputChangeHandler}
                                />
                                <InputGroupAddon addonType="append"><FormattedMessage id="required"/></InputGroupAddon>
                                <Input type="number" min="0" name="startPlaces" id="startPlaces" placeholder="start"
                                       value={this.state.startPlaces} onChange={this.inputChangeHandler}
                                />
                                <InputGroupAddon addonType="append"><FormattedMessage id="max"/></InputGroupAddon>
                                <Input type="number" min="0" name="maxPlaces" id="maxPlaces" placeholder="max"
                                       value={this.state.maxPlaces} onChange={this.inputChangeHandler}
                                />
                            </InputGroup>
                        </Col>
                    </FormGroup>

                    <FormElement
                        propertyName="priceMonth"
                        title={<FormattedMessage id="cost"/>}
                        type="number"
                        value={this.state.priceMonth}
                        onChange={this.inputChangeHandler}
                        placeholder={this.props.intl.formatMessage({id: 'enterCourseFee'})}
                        required min={0}

                    />
                    <FormElement
                        propertyName="description"
                        title={<FormattedMessage id="courseDescription"/>}
                        type="textarea"
                        value={this.state.description}
                        onChange={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="address"
                        title={<FormattedMessage id="address"/>}
                        type="text"
                        value={this.state.address}
                        onChange={this.inputChangeHandler}
                        required
                    />

                    <FormGroup>
                        <Map className="EventForm-Map" center={this.state.center}
                             zoom={16} onClick={this.mapClickHandler}>
                            <TileLayer
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                            />
                            {this.state.markerLocation && <Marker position={this.state.markerLocation}/>}
                        </Map>
                    </FormGroup>

                    <FormGroup>
                        <Col>
                            <Button type="submit" color="primary"><FormattedMessage id="add"/></Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

CreateEventForm.propTypes = {
    intl: intlShape.isRequired
};

const mapStateToProps = state => ({
    categories: state.categories.categories
});

const mapDispatchToProps = dispatch => ({
   fetchCategories: () => dispatch(fetchCategories())
});

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(CreateEventForm));