import React from 'react';
import {Button, Nav, Navbar, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import AnonymousMenu from "../Toolbar/Menus/AnonymousMenu";

import {FormattedMessage} from "react-intl";

const Footer = ({user, logout}) => {
    return (

        <Navbar color="dark" dark expand="md">
            <Nav className="ml-left" navbar>
                {!user || user.verification === true ?
                    <NavItem>
                         <NavLink tag={RouterNavLink} to="/" exact><FormattedMessage id="toMain"/></NavLink>
                    </NavItem>
                : <Button color='link' onClick={logout}>
                      <FormattedMessage id="exit"/>
                  </Button>
                }
                {user ? null : <AnonymousMenu/>}
            </Nav>
        </Navbar>

    );
};

export default Footer;