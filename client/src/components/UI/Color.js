import React, {Component} from 'react';

const createHSLFromHue = (hue) =>{
    return `hsl(${hue},65%,55%)`;
};

class Color extends Component {
    render() {

        let hue = 13;

        const colorNum = 50;

        const colors = [];

        for (let i=1;i<=colorNum;i++){
            let step = 67;
            colors.push(createHSLFromHue(hue));
            hue += step;

            if(hue>359){
                hue -= 359;
            }
        }

        return (
            <div>
                {
                 colors.map(color => (
                     <div
                         style={{"display":"inline-block",
                                 "margin-left":"5px","background-color" : color,
                                 "width": "50px","height":"90px"}}>

                     </div>
                 ))
                }
            </div>
        );
    }
}

export default Color;