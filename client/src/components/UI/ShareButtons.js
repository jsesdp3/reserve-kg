import React from 'react';
import {
    FacebookShareButton,
    LinkedinShareButton,
    TwitterShareButton,
    TelegramShareButton,
    VKShareButton,
    OKShareButton,
    FacebookIcon,
    TwitterIcon,
    TelegramIcon,
    LinkedinIcon,
    VKIcon,
    OKIcon,
} from 'react-share';

import config from '../../config';

import "./ShareButtons.css";

const ShareButtons = ({eventId}) => {
    const url = config.siteURL + '/events/' + eventId;

    return (
        <div className="ShareButtons-Component">
            <FacebookShareButton
                url={url}
            >
                <FacebookIcon
                    size={32}
                    round />
            </FacebookShareButton>
            <LinkedinShareButton
                url={url}
            >
                <LinkedinIcon
                    size={32}
                    round/>
            </LinkedinShareButton>
            <TwitterShareButton
                url={url}
            >
                <TwitterIcon
                    size={32}
                    round
                />
            </TwitterShareButton>
            <TelegramShareButton
                url={url}
            >
                <TelegramIcon
                    size={32}
                    round
                />
            </TelegramShareButton>
            <VKShareButton
                url={url}
            >
                <VKIcon
                    size={32}
                    round
                />
            </VKShareButton>
            <OKShareButton
                url={url}
            >
                <OKIcon
                    size={32}
                    round
                />
            </OKShareButton>
        </div>
    );
};

export default ShareButtons;