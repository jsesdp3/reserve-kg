import React, {Fragment} from 'react';
import {
    Button,
    Nav,
    Navbar,
    NavbarBrand, NavItem, NavLink,
} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import UserMenu from "./Menus/UserMenu";
import AnonymousMenu from "./Menus/AnonymousMenu";
import logo from "../../../assets/images/logo.png";
import LanguageSwitch from "../../../Translation/LanguageSwitch";

import './Toolbar.css';
import {FormattedMessage} from "react-intl";


const Toolbar = ({user, logout}) => {
    return (
    <Navbar color="light" light expand="md">
        {(!user || user.verification === true) ?
        <NavbarBrand tag={RouterNavLink} to="/"><img src={logo} alt='Logo' style={{height: '40px'}} /></NavbarBrand>
        : <img src={logo} alt='Logo' style={{height: '40px'}} />
        }
        <LanguageSwitch />
        {(!user || user.verification === true) ?
            <Nav className="ml-auto" navbar style={{alignItems: 'baseline'}}>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/" exact><FormattedMessage id="titleEvents"/></NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/organizations" exact><FormattedMessage id="allOrganizations"/></NavLink>
                </NavItem>
            {user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu/>}
            </Nav>
        : <Fragment>
                <Nav className="ml-auto" navbar style={{alignItems: 'baseline'}}>
                    <NavItem>
                        <NavLink tag={RouterNavLink} to={`/verification/${user._id}`} exact>Поддтвердите регистрацию</NavLink>
                    </NavItem>
                    <Button color='link' onClick={logout}>
                        <FormattedMessage id="exit"/>
                    </Button>
                </Nav>
            </Fragment>
        }
    </Navbar>
  );
};

export default Toolbar;
