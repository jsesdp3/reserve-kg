import React, {Fragment} from 'react';
import {NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import {FormattedMessage} from "react-intl";

const AnonymousMenu = () => (
      <Fragment>
        <NavItem>
          <NavLink tag={RouterNavLink} to="/register" exact><FormattedMessage id="registration"/></NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={RouterNavLink} to="/login" exact><FormattedMessage id="signIn"/></NavLink>
        </NavItem>

      </Fragment>
);

export default AnonymousMenu;
