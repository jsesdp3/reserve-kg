import React, {Fragment} from 'react';
import {Button, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import AvatarThumbnail from "../../../AvatarThumbnail/AvatarThumbnail";
import {FormattedMessage} from "react-intl";


const UserMenu = ({user, logout}) => {
    return (
        <Fragment>
            {user.verification === true ?
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret className='d-inline-block'>
                        <FormattedMessage id="hello"/>, {user.displayName}!
                    </DropdownToggle>
                    <AvatarThumbnail user={user}/>
                    <DropdownMenu right className="user-menu">
                        <DropdownItem tag={RouterNavLink} to="/my_calendar" exact>
                            <FormattedMessage id="myCalendar"/>
                        </DropdownItem>

                        {user.organization ? (
                            <DropdownItem tag={RouterNavLink} to={"/organizations/" + user.organization._id} exact>
                                <FormattedMessage id="myOrganization"/>
                            </DropdownItem>
                        ) : (
                            <DropdownItem tag={RouterNavLink} to={"/create_organization"} exact>
                                <FormattedMessage id="createOrganization"/>
                            </DropdownItem>
                        )}

                        <DropdownItem tag={RouterNavLink} to="/events/new" exact>
                            <FormattedMessage id="addCourse"/>
                        </DropdownItem>

                        {user.role === 'admin' && (
                            <Fragment>
                                <DropdownItem tag={RouterNavLink} to="/admin/users" exact>
                                    <FormattedMessage id="viewingUsers"/>
                                </DropdownItem>
                                <DropdownItem tag={RouterNavLink} to="/categories/management" exact>
                                    <FormattedMessage id="categoryManagement"/>
                                </DropdownItem>
                            </Fragment>
                        )}

                        <DropdownItem divider/>
                        <DropdownItem onClick={logout}>
                            <FormattedMessage id="exit"/>
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            : <Fragment>
                    <Button color='link' onClick={logout}>
                        <FormattedMessage id="exit"/>
                    </Button>
                </Fragment>
            }
        </Fragment>

    )

};

export default UserMenu;
