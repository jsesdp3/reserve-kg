import React, {Component, Fragment} from 'react';
import {FaBars} from "react-icons/fa";
import {
    Button,
    DropdownItem,
    DropdownMenu,
    DropdownToggle, Modal, ModalBody, ModalFooter, ModalHeader,
    UncontrolledButtonDropdown
} from "reactstrap";
import {FormattedMessage} from "react-intl";
import {NavLink as RouterNavLink, withRouter} from "react-router-dom";
import {deleteEvent, eventModeration, patchEvent} from "../../store/actions/eventsActions";
import {connect} from "react-redux";

class EventDropdown extends Component {
    state = {
        showDeleteModal: false
    };

    toggleDeleteModal = () => {
        this.setState({showDeleteModal: !this.state.showDeleteModal});
    };

    patchEvent = (id, status) => {
        this.props.patchEvent(id, status).then(() => {
            this.props.history.push('/')
        });
    };

    render() {
        const {event, user} = this.props;

        return (
            <Fragment>
                <div className="Event-dropdown-btn">
                    <UncontrolledButtonDropdown>
                        <DropdownToggle caret>
                            <FaBars/>
                        </DropdownToggle>
                        <DropdownMenu right>
                            {(user._id === event.user._id) &&
                            <DropdownItem
                                tag={RouterNavLink}
                                exact
                                to={`/events/${event._id}/edit`}
                            >
                                <FormattedMessage id="edit"/>
                            </DropdownItem>
                            }

                            {(event.status === 'unpublished' && user._id === event.user._id) && (
                                <DropdownItem onClick={() => this.props.eventModeration(event._id, 'on_moderation')}>
                                    <FormattedMessage id="sendForModeration"/>
                                </DropdownItem>
                            )}
                            {(user.role ==='admin'&& event.status === 'on_moderation') &&
                                <Fragment>
                                    <DropdownItem header>
                                        <strong className="text-danger"><FormattedMessage id="oNmoderation"/></strong>
                                    </DropdownItem>
                                    <DropdownItem
                                        onClick={() => this.patchEvent(event._id, 'unpublished')}
                                    >
                                        <FormattedMessage id="reject"/>
                                    </DropdownItem>
                                    <DropdownItem
                                        onClick={() => this.patchEvent(event._id, 'published')}
                                    >
                                        <FormattedMessage id="publish"/>
                                    </DropdownItem>
                                </Fragment>
                            }

                            <DropdownItem divider/>

                            <DropdownItem onClick={() => this.toggleDeleteModal(event._id)}>
                                <FormattedMessage id="delete"/>
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledButtonDropdown>
                </div>

                <Modal isOpen={this.state.showDeleteModal} toggle={this.toggleDeleteModal}>
                    <ModalHeader toggle={this.toggleDeleteModal}><FormattedMessage id="confirmations"/></ModalHeader>
                    <ModalBody>
                        <FormattedMessage id="youDefinitelyWantToDeleteThisCourse"/>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={() => this.props.deleteEvent(event._id)}>
                            <FormattedMessage id="yes"/>
                        </Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>
                            <FormattedMessage id="cancel"/>
                        </Button>
                    </ModalFooter>
                </Modal>

            </Fragment>
        );
    };
}

const mapDispatchToProps = dispatch => ({
    eventModeration: (id, status) => dispatch(eventModeration(id, status)),
    deleteEvent: id => dispatch(deleteEvent(id)),
    patchEvent: (id, status) => dispatch(patchEvent(id, status)),
});

export default  withRouter(connect(null, mapDispatchToProps)(EventDropdown));