import React, {Component} from 'react';
import {Button} from "reactstrap";
import GoogleLoginButton from 'react-google-login'
import {googleLogin} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import {NotificationManager} from "react-notifications";
import {FormattedMessage} from "react-intl";


class GoogleLogin extends Component {
    responseGoogle = res => {
        if (res.details) {
            NotificationManager.error(<FormattedMessage id="somethingWentWrong"/>)
        } else if(res.error && !res.name){
            NotificationManager.warning(<FormattedMessage id="youClickedCancel"/>);
        } else {
            this.props.googleLogin(res)
        }
    };

    render() {
        return (
            <GoogleLoginButton
                clientId="627349251064-snmds7kh98nhhbb9qab565f3d793dfj0.apps.googleusercontent.com"
                render={renderProps => (
                    <Button outline color="danger" className="ml-2" onClick={renderProps.onClick} disabled={renderProps.disabled}><FormattedMessage id="loginWithGoogle"/></Button>
                )}
                buttonText="Login"
                onSuccess={this.responseGoogle}
                onFailure={this.responseGoogle}
            />
        );
    }
}


const mapDispatchToProps = dispatch => ({
   googleLogin: userData => dispatch(googleLogin(userData))
});

export default connect(null, mapDispatchToProps)(GoogleLogin);