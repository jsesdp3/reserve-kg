const express = require ("express");
const fs = require('fs');
const axios = require('axios');
const cheerio = require('cheerio');

const port = process.env.PORT || 8088;

const path = process.env.INDEX_PATH || '../front/build/index.html';

const apiURL = process.env.API_URL || 'http://localhost:8000';

const app = express();


app.use("*", async (req,res) => {
    let file = fs.readFileSync(path);
    file = file.toString().replace(/%PUBLIC_URL%/gi, '');

    try {
        console.log(req.originalUrl);
        const match = req.originalUrl.match(/^\/events\/([a-z0-9]+)$/i);

        if (match) {
            const eventApiUrl = apiURL + '/events/' + match[1];
            const response = await axios.get(eventApiUrl);
            const eventData = response.data;
            const $ = cheerio.load(file);
            $('title').text(eventData.title);
            const head = $('head');
            head.append(`<meta property="og:title" content="${eventData.title}"/>`);
            head.append(`<meta property="og:description" content="${eventData.description}"/>`);
            head.append(`<meta property="og:image" content="${apiURL + '/uploads/' + eventData.image}"/>`);

            head.append(`<meta name="description" content="${eventData.description}"/>`);

            file = $.html();
        }

        res.set('content-type', 'text/html');

        return res.send(file);
    } catch (e) {
        if (e.response && e.response.status >= 400) {
            res.set('content-type', 'text/html');
            return res.send(file);
        }

        console.error(e);
        res.sendStatus(500);
    }
});

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});