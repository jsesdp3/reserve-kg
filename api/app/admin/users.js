const express = require('express');
const auth = require('../../middleware/auth');
const permit = require('../../middleware/permit');
const User = require('../../models/User');

const router = express.Router();

router.get('/', [auth, permit('admin')], async (req, res) => {
  try {
    const users = await User.find({}, {email: true, phone: true, displayName: true, role: true}).sort('role');

    return res.send(users)
  }catch (e) {
    return res.status(500).send(e)
  }
});

router.patch('/role_change/:id', [auth, permit('admin')], async (req, res) => {
  try {

    const user = await User.findById(req.params.id);

    user.role = req.body.role;

    await user.save();

    res.sendStatus(200)

  }catch (e) {
    return res.status(500).send(e)
  }
});

module.exports = router;