const express = require('express');

const tryAuth = require('../middleware/tryAuth');
const auth = require('../middleware/auth');
const Group = require('../models/Group');

const router = express.Router();

router.get('/', tryAuth, (req, res) => {
    Group.find()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
});

router.post('/', auth, (req, res) => {
    const groupInfo = req.body;

    const group = new Group(groupInfo);

    group.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

router.delete('/:id', [auth],(req,res)=>{
    Group.remove({_id: req.params.id})
        .then(result=>res.send(result))
        .catch(error=>res.status(403).send(error))
});

module.exports = router;