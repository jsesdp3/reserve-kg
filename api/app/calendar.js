const express = require('express');
const moment = require('moment');
const Group = require('../models/Group');
const Event = require('../models/Event');
const auth = require('../middleware/auth');
const COLORS =['#fdd65d','#d9336c','#28cf75','#00a9fe','#2f64e5'];


const createCalendarEventsForGroups = groups => {
    const calendarEvents = [];

    for(let g = 0; g < groups.length; g++) {

        const group = groups[g];
        const color = COLORS[g] || '#a13e97';
        let prevEvent = moment(group.event.startDate);

        const startingDay = prevEvent.isoWeekday();

        const groupWeekDays = group.weekDays
            .sort((a, b) => a > b ? 1 : -1);

        let dayIdx = groupWeekDays.findIndex(groupWeekDay => groupWeekDay >= startingDay);
        if (dayIdx === -1) dayIdx = 0;

        for (let i=1; i<=group.event.eventsNumber; i++) {
            const currDayNumber = groupWeekDays[dayIdx];
            const prevDayNumber = moment(prevEvent).isoWeekday();
            let currDate = null;

            if (currDayNumber >= prevDayNumber) {
                currDate = moment(prevEvent).isoWeekday(currDayNumber);
            } else {
                currDate = moment(prevEvent).add('1', 'week').isoWeekday(currDayNumber);
            }

            const time = group.time.split(':');
            currDate.set({hour: time[0], minute: time[1]});

            prevEvent = currDate;

            calendarEvents.push({
                groupTitle: group.name ,
                event: group.event,
                number: i,
                start: currDate,
                color
            });

            dayIdx++;

            if (dayIdx === group.weekDays.length) {
                dayIdx = 0;
            }
        }
    }

    return calendarEvents;
};

const router = express.Router();

router.get('/event/:id/', auth, async (req,res) => {
    try {
        const event = await Event.findById(req.params.id);
        if(!event.user.equals(req.user._id)){
            return res.sendStatus(403);
        }

        const groups = await Group.find({event: event._id}).populate("event");
        const calendarEvents = createCalendarEventsForGroups(groups);
        return res.send(calendarEvents)
    } catch(e) {
        console.error(e);
        return res.sendStatus(500);
    }

});

router.get('/my',auth, async (req,res)=>{
    try {
        const userGroups = await Group.find({users: req.user.id}).populate("event");
        const userCalendarEvents = createCalendarEventsForGroups(userGroups);
        return res.send(userCalendarEvents);
    } catch (e) {
        console.error(e);
        return res.sendStatus(500)
    }
});
module.exports = router;