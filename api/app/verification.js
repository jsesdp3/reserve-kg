const express = require('express');
const User = require('../models/User');
const nanoid = require("nanoid");
const generate = require('nanoid/generate');
const auth = require('../middleware/auth');
const router = express.Router();
const axios =require('axios');

router.post('/:id', auth, async (req, res) =>{
    const convert = require('xml-js');
    const idNanoid = nanoid(6);
    const verificationPassword = generate ('12345567890', 4);

    try {
        const userVer = await User.findById(req.body._id);

        userVer.verificationPassword = verificationPassword;

        const jsToXml = {
            _declaration: {
                _attributes: {
                    version: "1.0",
                    encoding: "utf-8"
                }
            },
            message: {
                login: {_text: "reserve"},
                pwd: {_text: "8O05AbUX"},
                id: {_text: idNanoid},
                sender: {_text: "996700035045"},
                text: {_text: verificationPassword},
                phones: {
                    phone: {
                        _text: userVer.phone
                    }
                },
                test: {_text: "1"},
            }
        };
        const options = {compact: true, ignoreComment: true, spaces: 0};
        const result = convert.js2xml(jsToXml, options);

        await userVer.save();
        axios.post('http://smspro.nikita.kg/api/message', result)
            .then(function (response) {
                return response;
            })
            .catch(function (error) {
                return error;
            });

        return res.status(200).send('ok');

    }catch (e) {
        return res.status(500).send(e)
    }
});

router.put('/:id', auth, async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        const userVerPass = parseInt(user.verificationPassword);
        const reqBodyCode = parseInt(req.body.verificationCode);

        if  (userVerPass === reqBodyCode){
            user.verification = true;
            user.save();
        }

        return res.status(200).send('ok');

    }catch (e) {
        return res.status(500).send(e)
    }

});

router.get('/:id', auth, async (req, res) => {
    try {
        const user = await User.findById(req.params.id);

        return res.send(user);

    }catch (e) {
        return res.status(500).send(e)
    }
});

module.exports = router;