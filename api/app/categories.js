const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Category = require('../models/Category');
const Event = require('../models/Event');

const  router = express.Router();

router.get('/', (req, res) => {
    Category.find()
        .then(categories => res.send(categories))
        .catch(() => res.sendStatus(500));
});

router.post('/', [auth, permit('admin')], (req, res) => {
    const category = new Category(req.body);

    category.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});


router.delete('/:id', [auth, permit('admin')], async (req, res) => {

    const event = await Event.find({category: req.params.id});

    if (event.length >= 1) {
       return res.status(404).send({message: 'У этой категорий есть курсы'})
    }

    const category = await Category.findByIdAndDelete({_id: req.params.id});

    category.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});


module.exports = router;