const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const sharp = require('sharp');
const tryAuth = require('../middleware/tryAuth');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const config = require('../config');
const Event = require('../models/Event');
const User = require('../models/User');
const Group = require('../models/Group');
const ObjectId = require('mongoose').Types.ObjectId;


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const getCriteriaForEventsVisibility = req => {
    let criteria = {status: 'published'};

    if(req.query.category) {
        criteria.category = ObjectId(req.query.category)
    }

    if (req.user && req.user.role === 'user' && !req.query.category) {
        criteria = {
            $or: [
                {status: 'published'},
                {user: req.user._id},
            ]
        }
    } else if (req.user && req.user.role === 'user' && req.query.category) {
        criteria = {
            category: ObjectId(req.query.category),
            $or: [
                {status: 'published'},
                {user: req.user._id},
            ]
        }
    } else if (req.user && req.user.role === 'admin' && req.query.category) {
        criteria = {category: ObjectId(req.query.category)}
    } else if (req.user && req.user.role === 'admin' && !req.query.category) {
        criteria = {}
    }

    return criteria;
};

router.get('/', tryAuth, async (req, res) => {
  try {
      const criteria = getCriteriaForEventsVisibility(req);

      if (req.query.lng && req.query.lat) {
          const lng = req.query.lng;
          const lat = req.query.lat;

          const events = await Event.aggregate([
              {
                  $geoNear: {
                      near: {
                          type: "Point",
                          coordinates: [ parseFloat(lng) , parseFloat(lat)]
                      },
                      distanceField: "dist.calculated",
                      maxDistance: 2000000,
                      spherical: true
                  },
              },
              {$match: criteria},
          ]);

          return res.send(events);
      } else {
        const events = await Event.find(criteria).populate('user', '_id, displayName').sort('_id');
        return res.send(events);
      }
  } catch (e) {
      console.log(e);
    return res.status(500).send(e)
  }

});

router.get('/by_organization/:id', tryAuth, async (req, res) => {
    try {
        const criteria = getCriteriaForEventsVisibility(req);
        criteria.organization = req.params.id;

        const events = await Event.find(criteria);
        return res.send(events);
    } catch (e) {
        console.log(e);
        return res.sendStatus(500);
    }
});

router.get('/:id',async (req,res)=>{
  try {
    const event = await Event.findById(req.params.id);

    const groups = await Group.find({event: req.params.id}).sort('_id');

    const eventGroup = {...event.toObject()};

    eventGroup.groups = groups;

    return res.send(eventGroup)
  }catch (e) {
    return res.status(500).send(e)
  }
});

router.get('/:id/groups',[tryAuth, permit('admin')],async (req,res) => {
  try {
    const event = await Event.findById(req.params.id);

      const groups = await Group.find({event:event}).populate('users', 'email phone displayName').sort('_id');

        for (let group of groups) {
            group.users = await User.find({_id: {$in: group.users}}, {email: true, phone: true, displayName: true});
        }
      const newEvent = {...event.toObject()};
      newEvent.groups = groups;
      return res.send(newEvent);
  }catch (e) {
    console.log(e);
    return res.status(500).send(e)
  }
});

router.post('/',[auth,upload.single('image')], async (req,res)=>{
  const eventData = req.body;

  eventData.user = req.user._id;

  if (req.user.organization) {
      eventData.organization = req.user.organization;
  }

  let thumbFile = nanoid() + path.extname(req.file.originalname);

  if (req.file) {
    eventData.image = req.file.filename;
    await sharp(req.file.path)
        .resize(400)
        .toFile(`${config.uploadPath}/thumb/${thumbFile}`);
      eventData.thumbImage = thumbFile;
  }

  if ('location' in eventData) {
      const coords = eventData.location.split(',');

      eventData.location = {
          type: 'Point',
          coordinates: [parseFloat(coords[1]), parseFloat(coords[0])]
      };
  }

  const event = new Event(eventData);

    await event.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
});

router.post('/:id/groups', auth, (req, res) => {
  const groupInfo = req.body;

  const group = new Group({
      name: groupInfo.name,
      weekDays: groupInfo.weekDays.map(s => parseInt(s, 10)).sort(),
      time: groupInfo.time,
      event: groupInfo.event
  });

  group.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error))
});

router.patch('/:id',[auth, permit('admin')],async (req, res) => {
  try {

    const event = await Event.findById(req.params.id);

    event.status = req.body.status;

    await event.save();

    res.sendStatus(200)

  }catch (e) {
    return res.status(500).send(e)
  }
});

router.put('/:id', [auth, permit('admin', 'user'), upload.single('image')], async (req, res) => {
  const singleEventData = {...req.body};
  singleEventData.user = req.user._id;

    if (req.file) {
        singleEventData.image = req.file.filename;
    }

    if (singleEventData.location) {
        console.log('LOCATION', singleEventData.location);
        const coords = singleEventData.location.split(',');

        singleEventData.location = {
            type: 'Point',
            coordinates: [parseFloat(coords[1]), parseFloat(coords[0])]
        };
    } else {
        delete singleEventData.location;
    }

    console.log(singleEventData);

    const singleEvent = await Event.findByIdAndUpdate(req.params.id, singleEventData);

  singleEvent.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
});

router.patch('/moderation_events/:id', auth, async (req, res) => {
    try {

        const event = await Event.findById(req.params.id);

        event.status = req.body.status;

        await event.save();

        res.sendStatus(200)

    }catch (e) {
        return res.status(500).send(e)
    }
});

router.patch('/submit/:id', auth, async (req, res) => {

    try {
    const group = await Group.findById(req.params.id);


    group.users.push(req.user._id);

    await group.save();

    res.sendStatus(200)

  }catch (e) {
    return res.status(500).send(e)
  }
});

router.delete('/:id', [auth],(req,res)=>{
    Event.findByIdAndDelete({_id: req.params.id})
      .then(result => res.send(result))
      .catch(error=>res.status(403).send(error))

});

router.delete('/:id/groups/:id', (req, res) => {
  Group.findByIdAndDelete(req.params.id)
      .then(result => res.send(result))
      .catch(error => res.status(403).send(error))
});



module.exports = router;