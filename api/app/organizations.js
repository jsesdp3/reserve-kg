const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const sharp = require('sharp');
const path = require('path');

const config = require('../config');

const tryAuth = require('../middleware/tryAuth');
const auth = require('../middleware/auth');

const Organization = require('../models/Organization');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
        filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/', tryAuth, (req, res) =>{
    Organization.find()
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
});

router.get('/:id', async (req, res) => {
    try {
        const org = await Organization.findById(req.params.id);

        if (!org) {
            return res.sendStatus(404);
        }
        return res.send(org);
    } catch (e) {
        return res.sendStatus(500);
    }
});

router.post('/', [auth, upload.fields([{name: 'logo', maxCount: 1}, {name: 'gallery', maxCount: 10}])], async (req, res) => {
    try {
        const existingOrganization = await Organization.findOne({owner: req.user._id});

        if (existingOrganization) {
            return res.status(400).send({message: 'You cannot have more than one organization'});
        }

        const organizationData = req.body;

        organizationData.owner = req.user._id;

        if (!organizationData.location) {
            return res.status(400).send({message: "No location specified"});
        }

        if (req.files) {
            organizationData.logo = req.files.logo[0].filename;
            await sharp(req.files.logo[0].path)
                .resize(300)
                .toFile(`${config.uploadPath}/thumb/${req.files.logo[0].filename}`);

            organizationData.gallery = req.files.gallery ? req.files.gallery.map(file =>  file.filename) : undefined;
        }

        const coords = organizationData.location.split(',');

        organizationData.location = {
            type: 'Point',
            coordinates: [parseFloat(coords[1]), parseFloat(coords[0])]
        };

        const organization = new Organization(organizationData);
        await organization.save();

        return res.send(organization);
    } catch (e) {
        console.log(e);
        return res.sendStatus(500);
    }
});

module.exports = router;