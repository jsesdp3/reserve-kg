const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const nanoid = require("nanoid");

const SALT_WORK_FACTOR = 10;

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async function(value) {
        if (!this.isModified('email')) return;

        const user = await User.findOne({email: value});

        if (user) throw new Error();
      },
      message: 'This email is already taken'
    }
  },
  password: {
    type: String,
    required: true
  },
  token: {
    type: String,
    required: true
  },
  facebookId: {
    type: String
  },
  googleId: {
    type: String
  },
  phone: {
    type: String
  },
  role: {
    type: String,
    default: 'user',
    enum: ['user', 'admin']
   },
  avatarImage: String,
  displayName: String,
  verification: {
    type: Boolean,
    default: false,
    required:true
  },
  verificationPassword: Number,

});

UserSchema.methods.checkPassword = function(password) {
  return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function() {
  this.token = nanoid();
};

UserSchema.pre('save', async function(next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  const hash = await bcrypt.hash(this.password, salt);

  this.password = hash;

  next();
});

UserSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    delete ret.password;
    delete ret.verificationPassword;
    return ret;
  },
  virtuals: true
});

UserSchema.virtual('organization', {
  ref: 'Organization',
  localField: '_id',
  foreignField: 'owner',
  justOne: true
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
