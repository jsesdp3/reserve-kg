const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    titleRu:{
        type:String,
        required:true
    },
    titleEn: {
        type: String,
        required: true
    },
    titleKy: {
        type: String,
        required: true
    }
});

const Category = mongoose.model('Category', CategorySchema);

module.exports = Category;