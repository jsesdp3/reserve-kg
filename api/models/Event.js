const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const EventSchema = new Schema({
    title:{
        type:String,
        required:true
    },
    image: String,
    thumbImage: String,
    startDate:{
        type: Date,
        required: true
    },
    minPlaces: Number,
    startPlaces: Number,
    maxPlaces: Number,
    user:{
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    description: String,
    address: String,
    location: {
        type: { type: String },
        coordinates: [Number],
    },
    status:{
        type: String,
        enum:['unpublished','on_moderation','published'],
        default: 'unpublished'
    },
    priceMonth: String,
    eventsNumber:Number,
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    },
    organization:{
       type: Schema.Types.ObjectId,
       ref: 'Organization'
    }
});

EventSchema.index({ "location" : "2dsphere" });

const Event = mongoose.model('Event', EventSchema);


module.exports = Event;