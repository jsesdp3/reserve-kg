const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const OrganizationSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    description: {
        type: String,
        required: true
    },
    address: String,
    location: {
        type: { type: String },
        coordinates: [Number],
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    logo: String,
    gallery: [String],
});

const Organization = mongoose.model('Organization', OrganizationSchema);

module.exports = Organization;