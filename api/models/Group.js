const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const GroupSchema = new Schema({
   name: {
       type: String,
       required:true
   },
    weekDays: [Number],
    time: {
       type: String,
        required: true
    },
    users: {
            type: [{type: Schema.ObjectId, ref: 'User'}],
            default: [],
        },
    event :{
        type: Schema.Types.ObjectId,
        ref: 'Event',
        required: true
    },
});

const Group = mongoose.model('Group', GroupSchema);

module.exports = Group;