const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const users = require('./app/users');
const events = require('./app/events');
const groups = require('./app/groups');
const calendar = require('./app/calendar');
const categories = require('./app/categories');
const organizations = require('./app/organizations');

const verification = require("./app/verification");
const adminUsers = require('./app/admin/users');


const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

let port = 8000;

if (process.env.NODE_ENV === 'test') {
  port = 8010;
} else if (process.env.NODE_ENV === 'production') {
  port = 8008;
}


mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/admin/users', adminUsers);
  app.use('/users', users);
  app.use('/events', events);
  app.use('/groups', groups);
  app.use('/calendar', calendar);
  app.use('/categories', categories);
  app.use('/organizations',organizations);
  app.use('/verification', verification);


  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
});
