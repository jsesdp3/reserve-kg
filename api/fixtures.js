const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');



const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const User = require('./models/User');
    const Category = require('./models/Category');
    const Event = require('./models/Event');
    const Group = require('./models/Group');
    const Organization = require('./models/Organization');


  const user = await User.create(
      {
          email: 'John.Week@gmail.com',
          password: '123',
          token: nanoid(),
          displayName: 'Killer',
          avatarImage: 'john-wick.jpg',
          phone: '+996555332255',
          verification: true
      },
      {
          email: 'Ad.Haizenberg@gmail.com',
          password: '123',
          token: nanoid(),
          displayName: 'Haizenberg',
          avatarImage: 'admin.jpeg',
          phone: '+996553533444',
          role: 'admin',
          verification: true
      },
      {
          email: 'testuser1@gmail.com',
          password: '123',
          token: nanoid(),
          displayName: 'test1-user',
          avatarImage: 'john-wick.jpg',
          phone: '+996553111213',
          verification: true
      },
      {
          email: 'some1@gmail.com',
          password: '123',
          token: nanoid(),
          displayName: 'some1',
          avatarImage: 'admin.jpeg',
          phone: '+996553533446',
          verification: true
        },
        {
            email: 'some2@gmail.com',
            password: '123',
            token: nanoid(),
            displayName: 'some2',
            avatarImage: 'admin.jpeg',
            phone: '+996553533445',
            verification: true
        },

        {
            email: 'someTest1@gmail.com',
            password: '123',
            token: nanoid(),
            displayName: 'someTest1',
            avatarImage: 'admin.jpeg',
            phone: '+996553533446',
            verification: true
        },
        {
            email: 'someTest2@gmail.com',
            password: '123',
            token: nanoid(),
            displayName: 'someTest2',
            avatarImage: 'admin.jpeg',
            phone: '+996553533447',
            verification: true
        },
        {
            email: 'someTest3@gmail.com',
            password: '123',
            token: nanoid(),
            displayName: 'someTest3',
            avatarImage: 'admin.jpeg',
            phone: '+996553533448',
            verification: true
        },
      {
          email: 'makerskg@gmail.com',
          password: '123',
          token: nanoid(),
          displayName: 'Makers',
          avatarImage: 'makers.jpg',
          phone: '+996555332255',
          verification: true
      },
      {
          email: 'getup_kg@gmail.com',
          password: '123',
          token: nanoid(),
          displayName: 'GetUP',
          avatarImage: 'getup.jpg',
          phone: '0509-510-509',
          verification: true
      },
    );

    const organization = await Organization.create(
        {
            name: 'CodeCentral',
            description: 'Центр передового обучения программистов.Ваш путь к успеху,начинается у нас',
            address: 'Тыныстанова 8/1',
            location: {type: "Point", coordinates: [74.6035363, 42.8443635]},
            owner: user[1],
            logo: 'codecentral.png'
        },
        {
            name: 'Marry Pop Pins',
            description: 'Курсы для леди,что стремятся к совершенству',
            address: 'Тыныстанова 8/1',
            location: {type: "Point", coordinates: [74.6035363, 42.8443635]},
            owner: user[0],
            logo: 'marrypoppins.jpg'
        },
        {
            name: 'Makers Bootcamp',
            description: 'Мы научим Вас программированию за 2 месяца с нуля. ' +
                'Это не просто курсы программирования, у нас нет теории. ' +
                'Bootcamp – это лагерь программистов, предполагает интенсивный практический формат обучения, в котором студенты учатся, решая реальные задачи по программированию от наших партнеров. ' +
                'Партнерами Makers Bootcamp являются Beeline, Ololo, Парк Высоких Технологий, Colibri, Friday Tech, Namba Soft и др. ' +
                'Обучение ведется каждый день по 5 часов. Программа включает в себя хакатоны, мероприятия, а также знакомства с компаниями-партнерами. ' +
                'Вы учитесь программированию, решая реальную задачу от наших партнеров, выполняя проект или разрабатывая продукт.',
            address:'Боконбаева 113',
            location:{type: "Point",coordinates:[74.605776,42.881914]},
            owner: user[8],
            logo:'makers.jpg'
        },
        {
            name: 'GetUp',
            description: 'Групповые занятия по Химии и Математике в Бишкеке.',
            address:'Табышалиева 29',
            location:{type: "Point",coordinates:[74.586734,42.871584]},
            owner: user[9],
            logo:'getup.jpg'
        },
    );

    const category = await Category.create(
        {titleRu: 'Программирование', titleEn: 'Programming', titleKy: 'Программалоо'},
        {titleRu: 'Механика', titleEn: 'Mechanics', titleKy: 'Механикасы'},
        {titleRu: 'Химия', titleEn: 'Chemistry', titleKy: 'Химия'},
        {titleRu: 'Шитьё', titleEn: 'Stitching', titleKy: 'Тикмечиликте'}
    );

    const event = await Event.create(
        {
            title: 'JavaScript курсы. Научись программировать с нуля',
            image: 'javascript.png',
            thumbImage: 'javascript.png',
            startDate: '2019-06-25',
            minPlaces: 7,
            startPlaces: 10,
            maxPlaces: 15,
            user: user[1],
            description: '16 интенсивных занятий по вечерам в удобное время. ' +
                'Мало теории, много практики. Звоните! Полный курс по JavaScript в Бишкеке.',
            address: 'ул. Малдыбаева 7/1',
            location: {type: "Point", coordinates: [74.597247, 42.846378]},
            status: 'unpublished',
            priceMonth: '15000',
            eventsNumber: 10,
            category: category[0]._id,
            organization: organization[0]._id
        },
        {
            title: 'Теоретическая механика для инженеров и исследователей',
            image: 'teormeh.jpeg',
            thumbImage: 'teormeh.jpeg',
            startDate: '2019-08-29',
            minPlaces: 12,
            startPlaces: 17,
            maxPlaces: 25,
            user: user[0],
            description: 'В курсе рассматриваются: кинематика точки и твёрдого тела (причём с разных точек зрения ' +
                'предлагается рассмотреть проблему ориентации твердого тела), классические задачи динамики механических ' +
                'систем и динамики твердого тела, элементы небесной механики, движение систем переменного состава, теория' +
                ' удара, дифференциальные уравнения аналитической динамики.',
            address: 'пр. Мира 66',
            location: {type: "Point", coordinates: [74.5856504, 42.8449689]},
            status: 'published',
            priceMonth: '5000',
            eventsNumber: 20,
            category: category[1]._id
        },
        {
            title: 'Химия-курсы для всех возрастов',
            image: 'chemistry.jpg',
            thumbImage: 'chemistry.jpg',
            startDate: '2019-07-22',
            minPlaces: 10,
            startPlaces: 12,
            maxPlaces: 18,
            user: user[0],
            description: 'Программа преподавания включает в себя комплекс семестровых курсов, ' +
                'соответствующих классам в школе. Начать обучение можно с любого курса. После бесплатного ' +
                'пробного тестирования школьник попадет в группу своего уровня знаний.',
            address: 'ул. Логвиненко 55',
            location: {type: "Point", coordinates: [74.5990794, 42.8817191]},
            status: 'on_moderation',
            priceMonth: '12000',
            eventsNumber: 30,
            category: category[2]._id
        },
        {
            title: 'Карточка для автотеста по редактированию курса',
            image: 'javascript.png',
            thumbImage: 'javascript.png',
            startDate: '2019-06-25',
            minPlaces: 7,
            startPlaces: 10,
            maxPlaces: 15,
            user: user[1],
            description: 'Тест, тест, тест, тест, тест, тест, тест, много тестов',
            address: 'ул. Малдыбаева 7/1',
            location: {type: "Point", coordinates: [74.597247, 42.846378]},
            status: 'published',
            priceMonth: '15000000',
            eventsNumber: 30,
            category: category[0]._id
        },
        {
            title: 'Курсы по iOS разработке',
            image: 'ios.jpg',
            thumbImage: 'ios.jpg',
            startDate: '2019-06-26',
            minPlaces: 10,
            startPlaces: 12,
            maxPlaces: 18,
            user: user[1],
            description: 'Этот курс поможет вам быстро освоить азы разработки мобильных приложений под iOS.',
            address: 'ул. Логвиненко 55',
            location: {type: "Point", coordinates: [74.5990794, 42.8817191]},
            status: 'unpublished',
            priceMonth: '12000',
            eventsNumber: 10,
            category: category[0]._id,
            organization: organization[0]._id
        },
        {
            title: 'Курс по веб разработки ',
            image: 'php.jpg',
            thumbImage: 'php.jpg',
            startDate: '2019-07-26',
            minPlaces: 10,
            startPlaces: 12,
            maxPlaces: 18,
            user: user[4],
            description: '6 интенсивных занятий по вечерам в удобное время. \' +\n' +
                '        \'Мало теории, много практики. Звоните! Полный курс по php в Бишкеке.',
            address: 'ул. Малдыбаева 7/1',
            location: {type: "Point", coordinates: [74.597247, 42.846378]},
            status: 'published',
            priceMonth: '15000',
            eventsNumber: 30,
            category: category[0]._id,
        },
        {
            title: 'Python/Django',
            image: 'backend.svg',
            thumbImage: 'backend.svg',
            startDate: '2019-09-09',
            minPlaces: 25,
            startPlaces: 25,
            maxPlaces: 25,
            user: user[8],
            description: '48 интенсивных занятий по вечерам в удобное время. ' +
                'Мало теории, много практики.',
            address: 'ул. Малдыбаева 7/1',
            location: {type: "Point",coordinates:[74.605776,42.881914]},
            status: 'published',
            priceMonth: '5000',
            eventsNumber: 48,
            category: category[0]._id,
            organization: organization[2]._id
        },
        {
            title: 'React',
            image: 'react.svg',
            thumbImage: 'react.svg',
            startDate: '2019-08-26',
            minPlaces: 20,
            startPlaces: 20,
            maxPlaces: 20,
            user: user[8],
            description: '48 интенсивных занятий по вечерам в удобное время. ' +
                'Мало теории, много практики.',
            address: 'ул. Малдыбаева 7/1',
            location: {type: "Point",coordinates:[74.605776,42.881914]},
            status: 'published',
            priceMonth: '5000',
            eventsNumber: 48,
            category: category[0]._id,
            organization: organization[2]._id
        },
        {
            title: 'Неорганическая химия',
            image: 'getup_chem.jpg',
            thumbImage: 'getup_chem.jpg',
            startDate: '2019-09-22',
            minPlaces: 10,
            startPlaces: 12,
            maxPlaces: 18,
            user: user[9],
            description: 'Курсы по неорганической химии в Бишкеке. Разбор популярных задач,подготовка к экзаменам.',
            address: 'Табышалиева/29',
            location: {type: "Point", coordinates: [74.586734,42.871584]},
            status: 'published',
            priceMonth: '1200',
            eventsNumber: 30,
            category: category[2]._id,
            organization: organization[3]._id
        },
    );
    await Group.create(
        {
            name: 'Первая группа',
            weekDays: [1, 3, 5],
            time: '19:00',
            users: [user[0]._id, user[1]._id, user[3]._id, user[4]._id],
            event: event[0]
        },
        {
            name: 'Первая группа',
            weekDays: [1, 3, 5],
            time: '19:00',
            users: [user[3]._id],
            event: event[1]
        },
        {
            name: 'Вторая группа',
            weekDays: [2, 4, 6],
            time: '18:00',
            users: [user[5]._id, user[6]._id, user[7]._id],
            event: event[0]
        },
        {
            name: 'Первая группа',
            weekDays: [1, 3, 5],
            time: '19:00',
            users: [user[0]._id, user[1]._id, user[3]._id, user[4]._id],
            event: event[4]
        },
        {
            name: 'Первая группа',
            weekDays: [1,2, 3,4, 5,6],
            time: '9:00',
            users: [user[0]._id, user[1]._id, user[3]._id, user[4]._id],
            event: event[6]
        },
        {
            name: 'Первая группа',
            weekDays: [1,2, 3,4, 5,6],
            time: '9:00',
            users: [user[0]._id, user[1]._id, user[3]._id, user[4]._id],
            event: event[7]
        },
        {
            name: 'Первая группа',
            weekDays: [1, 3, 5],
            time: '19:00',
            users: [user[0]._id, user[1]._id, user[3]._id, user[4]._id],
            event: event[8]
        },
    );

    return connection.close();
};

run().catch(error => {
    console.error('Something wrong happened...', error);
});
