const path = require('path');

const rootPath = __dirname;

const dbUrl = process.env.NODE_ENV === 'test' ? 'mongodb://localhost/reserve_test': 'mongodb://localhost/reserve';

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl,
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '390672758455990',//изменить для регистрации через fb
    appSecret: 'e9094343954fa615fda6b5703174ae4e' // insecure! изменить для регистрации через fb
  }
};
