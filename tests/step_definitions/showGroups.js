const { I } = inject();
// Add in your custom step files

Given('я авторизованный пользователь {string}', name => {
    I.loginAsUser(name);
});

When('я вижу на странице текст {string}', (text) => {
    I.waitForVisible(`//h2[contains(.,'${text}')]`);
});

When('я вижу и нажимаю на кнопку {string}', (buttonName) => {
    I.click(`//a[.='${buttonName}']`)
});
+

Then('я вижу оповещение ', text => {
    I.seeNotification(text);
});



