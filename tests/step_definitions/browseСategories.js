const { I } = inject();
// Add in your custom step files

Given('я нахожусь на главной странице {string}', () => {
    I.amOnPage(link.home)
});

When('я выбираю категорию {string}', (text) => {
    I.click(`//a[contains(text(), '${text}')]`)
});

Then('я вижу текст {string}', (text) => {
    I.waitForElement(`//h2[contains(., '${text}')]`);
});
