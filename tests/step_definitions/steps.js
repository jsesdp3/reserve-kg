const { I } = inject();
// Add in your custom step files

When('я нахожусь на главной странице', () => {
  I.amOnPage('/');
  I.wait(2);
});

Then('я вижу текст {string}', (text) => {
  I.see(text);
});

When('я вижу картинку', () => {
  I.seeElement('#Event')
});

Before(async () => {
  I.replaceGeoLocationCallback();
});