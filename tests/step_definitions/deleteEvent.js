const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
    I.loginAsUser(name);
});

When('жму на кнопу Удалить курс {string}', (title) => {
    I.wait(2);
    I.click(`//h2[contains(.,'${title}')]/ancestor::div[@class='card-body']/descendant::button[.='Удалить']`);
});

Then('вижу Модальное окно c текстом {string}', (text) => {
    I.waitForElement(`//h5[contains(., '${text}')]`);
});

When('жму на кнопку {string}', (buttonText) => {
    I.wait(2);
    I.click(`//button[contains(.,'${buttonText}')]`);
});

When('я вижу оповещение {string}', (text) => {
    I.seeNotification(text);
});