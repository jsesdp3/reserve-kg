const { I } = inject();
// Add in your custom step files

Then('я вижу что общий доход {string} сом', (profit) => {
    I.waitForVisible(`//p[contains(.,'Общий доход от курса: ${profit} сом')]`, 30);
});