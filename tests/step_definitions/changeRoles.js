const { I } = inject();
// Add in your custom step files

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я нажимаю на выпадающий список {string}', () => {
  I.wait(2);
  I.click(button.menuUser);
  I.wait(2);
  I.click(button.viewListUsers);
});

When('я нахожу пользователя "Killer" и меняю роль на "admin"', () => {
  I.waitForVisible("//td[.='Killer']");
  I.click(`//td[.='Killer']/ancestor::tr/descendant::label[.='admin']`);
});

Then('я вижу оповещение {string}', (text) => {
  I.seeNotification(text);
});