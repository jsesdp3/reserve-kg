const { I } = inject();
// Add in your custom step files

Given('я авторизованный пользователь {string}', name => {
    I.loginAsUser(name);
});


Then('я нажимаю на кнопку "Опубликовать"', ()=> {
  I.wait(2);
  I.click(button.onModeration);
  I.wait(2);
  I.click(button.published);
});

Then(' я вижу оповещение {string}', (text) => {
    I.seeNotification(text);
});

