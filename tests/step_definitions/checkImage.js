const { I } = inject();
// Add in your custom step files

Given('я авторизованный пользователь {string}', name => {
    I.loginAsUser(name);
});

When('я вижу на странице название курса {string}', (text) => {
    I.waitForVisible(`//h2[contains(.,'${text}')]`, 30);
});

When('я вижу на странице сокращенную версию картинки для этого курса', () => {
   I.waitForElement(`//img[contains(@src, 'uploads/thumb/javascript.png')]`)
});

When('я нажимаю на кнопку для просмотра курса {string}', (text) => {
    I.clickViewCourses(text);
});

When('я вижу полную версию картинки курса', () => {
   I.waitForVisible(`//img[contains(@src, 'uploads/javascript.png')]`, 30)
});