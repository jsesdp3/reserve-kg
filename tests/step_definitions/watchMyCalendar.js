const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
    I.loginAsUser(name);
});

When('я нахожусь на главной странице, нажимаю в меню на "Мой календарь"',() =>{
    I.amOnPage(link.home);
    I.click(button.menuUser);
    I.click(button.myCalendar);
});

When('я нажимаю на событие {string} от {string}',(eventName, date)=>{
    I.click(`//a[@data-date=\'${date}\']//span[.="${eventName}"]`);
});

When('я вижу поп-ап с информацией о событии',()=>{
    I.waitForText('JavaScript курсы. Научись программировать с нуля');
    I.waitForText('Группа: Первая группа');
    I.waitForText('26.06.2019 в 19:00', 10);
    I.waitForText('ул. Малдыбаева 7/1');
});