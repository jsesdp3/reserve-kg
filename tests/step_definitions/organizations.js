const { I } = inject();

When('я нажимаю на {string} в меню пользователя', menuItemText => {
    const navbarMenu = `//*[contains(@class, 'navbar-nav')]`;
    I.click(navbarMenu + `//a[contains(@class, 'dropdown-toggle')]`);
    I.wait(3);
    I.click(navbarMenu + `//a/span[.="${menuItemText}"]/..`);
});

When('я нахожусь на странице созданной организации и вижу:', table => {
    for (const id in table.rows) {
        if (id < 1) {
            continue;
        }

        const cells = table.rows[id].cells;

        const name = cells[0].value;
        const value = cells[1].value;

        switch (name) {
            case 'name':
                I.waitForVisible(`//h2[.="${value}"]`, 5);
                break;
            case 'description':
                I.waitForVisible(`//p[contains(text(), "${value}")]`, 5);
                break;
        }
    }
});

When('я устанавливаю галочку {string}', title => {
    I.click(`//label/span[contains(text(), "${title}")]/../input`);
});

When('я нахожусь на странице всех организаций', () => {
    I.amOnPage(link.organizations);
});

When('я перехожу к организации {string}', orgName => {
    I.click(`//h2[contains(text(), '${orgName}')]/ancestor::div[@class="card-body"]/a`);
    I.waitForText(orgName, 2);
});

When('я вижу курс {string}', name => {
   I.waitForVisible(`//h2[contains(text(), "${name}")]`);
});
