const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
    I.loginAsUser(name);
});

When('я нахожусь на странице курса {string}',(eventName) =>{
    I.amOnPage(link.home);
    I.clickViewCourses(eventName);
});

When('я вижу и нажимаю на кнопку {string}', () => {
    I.click(button.viewCalendar);
});

When('я вижу календарь с событиями:', table =>{
    for(const id in table.rows){
        if(id<1){
            continue;
        }
        const cells = table.rows[id].cells;

        const date = cells[0].value;
        const time = cells[1].value;
        const name = cells[2].value;

        I.waitForVisible(`//a[@data-date='${date}']//span[.="${time}"]`,10);
        I.waitForVisible(`//a[@data-date='${date}']//span[.="${name}"]`,10);
    }
});