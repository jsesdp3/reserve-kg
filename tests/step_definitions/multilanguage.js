const { I } = inject();
// Add in your custom step files

When('я нахожусь на главной странице', () => {
    I.amOnPage('/');
    I.wait(2);
});

When('я вижу заголовок {string}', (text) => {
    I.waitForVisible(`//h1[.="${text}"]`);
});

When('я нажимаю на кнопку {string}', () => {
    I.click(`//*[@id="root"]/header/nav/button[1]`)
});