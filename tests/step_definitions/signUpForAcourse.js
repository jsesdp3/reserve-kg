const { I } = inject();
// Add in your custom step files

When('я захожу просмотреть курс {string}', (text) => {
  I.clickViewCourses(text);
});

Then('я проверяю что свободных мест {string} и записываюсь в группу {string} и вижу оповещение {string} и что количество свободных мест стало {string}', (freePlacesBefore, group, text, freePlacesAfter) => {
  // I.wait(5);
  I.waitForVisible(`//*[@id="root"]/div[2]/div[1]/strong/span`, `: ${freePlacesBefore}`);
  I.click(`//*[@id="root"]/div[2]/table/tbody/tr/td[1]/div/div/div/label`);
  I.click(button.signUp);
  I.seeNotification(text);
  I.waitForVisible(`//*[@id="root"]/div[2]/div[1]/strong/span`, `: ${freePlacesAfter}`)
});