const { I } = inject();
// Add in your custom step files

Given('я авторизованный пользователь {string}', name => {
    I.loginAsUser(name);
});

When('я вижу на странице название курса {string}', (text) => {
    I.waitForVisible(`//h2[contains(.,'${text}')]`, 30);
});

When('я нажимаю на кнопку для просмотра курса по редактированию {string}', (text) => {
    I.clickViewCourses(text);
});

When('я вижу и нажимаю на кнопку "Редактировать" для изменения ивента', () => {
    I.wait(5);
    I.click(button.editEvent);
});

When('я вижу заголовок страницы {string} для редактирования', (title) => {
    I.waitForVisible(`//h4[.='${title}']`);
});

When('я меняю заголовок курса {string}', (title) => {
    I.fillField(`//input[@id='title']`, title);
});

When('выбираю категорию {string}', (name) => {
  I.selectOption("//select[@name='category']", name)

});

When('я нажимаю на кнопку {string} для сохранения изменений', () => {
    I.click(button.saveEvent);
});

When('я выбираю пункт меню {string} у курса {string}', (item, courseId) => {
    const parentSelector = `//h2[contains(.,'${eventData[courseId]}')]/ancestor::div[@class='card-body']/descendant::`;
    const dropdownSelector = `button[contains(@class, "dropdown-toggle")]`;
    I.click(parentSelector + dropdownSelector);
    I.click(parentSelector + `div[contains(@class, "dropdown-menu")]//span[.="${item}"]`);
});

Then('я вижу новый заголовок для курса {string}', (title) => {
    I.wait(5);
   I.waitForVisible(`//h2[.='${title}']`);
});