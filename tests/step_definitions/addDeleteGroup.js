const { I } = inject();
// Add in your custom step files

Given('я авторизованный пользователь {string}', name => {
    I.loginAsUser(name);
});

When('я захожу просмотреть курс {string}', (text) => {
    I.clickViewCourses(text);
});

When('я вижу и нажимаю на кнопку "Добавить группу"', () => {
   I.click(button.addGroup);
});

When('я ввожу в поле формы "Введите название группы" название группы {string}', (text) => {
  I.fillField(formGroup.title, text);
});

When('я выбираю день недели {string}', () => {
   I.click(`//*[@id="root"]/div[2]/div[2]/form/div[2]/div/div[1]/label/span`)
});

When("я ввожу в поле формы времени часы проведения занятий {string}", (text) => {
    I.fillField(`//input[@name='hour24']`, text)
});

When("я ввожу в поле формы времени минуты проведения занятий {string}", (text) => {
    I.fillField(`//input[@name='minute']`, text)
});

When('после введения данных нажимаю на кнопку {string}', () => {
  I.click(button.addedGroup);
});

When('я вижу созданную группу {string} на странице групп', (groupName) => {
   I.waitForElement(`//div[@class='card-header' and text()='${groupName}']`, 30)
});

When('я кликаю на кнопку "Удалить"', () => {
   I.click(`//p[.='Тестовая группа']/ancestor::div[@class='card-header']/descendant::${button.deleteGroup}`);
});

Then('я вижу оповещение {string}', (text) => {
  I.seeNotification(text);
});