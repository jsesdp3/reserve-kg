const { I } = inject();

Given('нахожусь на главной странице', () => {
    I.amOnPage(link.home)
});

When('я кликаю по кнопке {string}', () => {
    I.click(`//${button.viewCourse}`);
});

When('я вижу полную информацию по ивенту',()=>{
    I.seeElement('//div[@class="container"]');
    I.seeElement('//h2[@class="EventPage-title"]');
    I.seeElement('//p[@class="EventPage-startdate"]');
    I.seeElement('//p[@class="EventPage-priceMonth"]');
    I.seeElement('//div[@class="EventPage-description"]');
});