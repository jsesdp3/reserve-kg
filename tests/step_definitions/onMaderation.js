const { I } = inject();
// Add in your custom step files

Given('я авторизованный пользователь {string}', name => {
    I.loginAsUser(name);
});

When('я жму на кнопку {string}', () => {
  I.wait(2);
  I.click(button.sendOnModeration);
});

Then(' я вижу оповещение {string}', (text) => {
    I.seeNotification(text);
});

