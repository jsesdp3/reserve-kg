const { I } = inject();
// Add in your custom step files

When('вижу кнопку {string} и нажимаю', (text) => {
    I.wait(2);
    I.click(`//span[.='${text}']`);
});

Then('я вижу в карточке дополнительный текст {string}', (distanceText) => {
    I.waitForElement(`//span[contains(.,'${distanceText}')]`);
    I.wait(3);
});