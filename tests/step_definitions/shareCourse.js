const { I } = inject();


When('нажимаю на кнопку поделиться в {string}', buttonName =>{
    const shareButtons = {
      "Linkedin": `//div[@aria-label="linkedin"]`
    };

    I.click(shareButtons[buttonName]);
    I.wait(10);
});

When('я вижу модальное окно {string}', modalName => {
    const modalXpaths = {
        "Linkedin": '//title[.="LinkedIn"]'
    };

    I.switchToNextTab();
    I.seeElementInDOM(modalXpaths[modalName]);
});

When('я перехожу по SSR-ссылке на событие {string}', async eventName => {

    const title = eventData[eventName];

    let url = await I.grabAttributeFrom(`//h2[contains(.,'${title}')]/ancestor::div[@class='card-body']/descendant::a`, 'href');

    url = url.replace('http://localhost:3010', 'http://localhost:8098');

    await I.amOnPage(url);
});

When('я вижу в коде страницы теги:', table => {
    for(const id in table.rows){
        if(id<1){
            continue;
        }
        const cells = table.rows[id].cells;

        const property = cells[0].value;
        const content = cells[1].value;

        I.seeInSource(`<meta property="${property}" content="${content}`); // <meta "og:title" "Теоретическая механика"/>
    }
});