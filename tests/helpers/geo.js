const Helper = codeceptjs.helper;

class GeoHelper extends Helper {
    async replaceGeoLocationCallback() {
        const page = this.helpers['Puppeteer'].page;

        await page.evaluateOnNewDocument(function() {
            navigator.geolocation.getCurrentPosition = function (cb) {
                setTimeout(() => {
                    cb({
                        'coords': {
                            accuracy: 21,
                            altitude: null,
                            altitudeAccuracy: null,
                            heading: null,
                            latitude: 42.8508692,
                            longitude: 74.6048947,
                            speed: null
                        }
                    })
                }, 1000)
            }
        });

    }
}

module.exports = GeoHelper;