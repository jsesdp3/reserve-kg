# language: ru

Функция: Отправить на модерацию
  @moderation
  Сценарий: Успешна отправить на модерацию
    Допустим я авторизованный пользователь "Haizenberg"
    И я выбираю пункт меню "Отправить на модерацию" у курса "JavaScript"
    То я вижу оповещение "Вы успешно отправили на модерацию"
