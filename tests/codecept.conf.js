exports.config = {
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://localhost:3010',
      show: !process.env.CI,
      headless: !!process.env.CI,
      windowSize: "1024x768",
    },
    LoginHelper: {
      require: './helpers/login.js',
    },
    GeoHelper: {
      require: './helpers/geo.js'
    },
    WordBookHelper: {
      require: './wordBook/wordBook.js'
    }
  },
  include: {
    I: './steps_file.js'
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: [
        './wordBook/wordBook.js',
        './step_definitions/steps.js',
        './step_definitions/register.js',
        './step_definitions/login.js',
        './step_definitions/published.js',
        './step_definitions/createEvent.js',
        './step_definitions/watchSingleEvent.js',
        './step_definitions/onMaderation.js',
        './step_definitions/addDeleteGroup.js',
        './step_definitions/showGroups.js',
        './step_definitions/signUpForAcourse.js',
        './step_definitions/watchCalendar.js',
        './step_definitions/changeRoles.js',
        './step_definitions/editEvent.js',
        './step_definitions/profitCalculation.js',
        './step_definitions/watchMyCalendar.js',
        './step_definitions/deleteEvent.js',
        './step_definitions/shareCourse.js',
        './step_definitions/browseСategories.js',
        './step_definitions/addDeleteCategory.js',
        './step_definitions/checkImage.js',
        './step_definitions/multilanguage.js',
        './step_definitions/organizations.js',
        './step_definitions/locationSearch.js'
    ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    }
  },
  tests: './*_test.js',
  name: 'tests'
};